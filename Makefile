-include .make/base.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/k8s.mk

# Temporary till project is renamed:
OCI_IMAGE = ska-portal

PREFIX=
APP=$(shell echo $$APP)
NX_BASE=main
NX_HEAD=HEAD
DEPLOYMENT_URL=172.29.246.190
NAMESPACE=/home

# ci deployment variables for target
IMAGE_REPO ?= registry.gitlab.com/ska-telescope/ska-portal
CHART ?= ska-portal
K8S_CHART ?= ska-portal
K8S_CHARTS ?= ska-portal
HELM_CHARTS_TO_PUBLISH ?= ska-portal
K8S_UMBRELLA_CHART_PATH ?= charts/ska-portal
HELM_RELEASE ?= ska-portal
KUBE_NAMESPACE ?= ska-portal
IMAGE_TAG_EXTRA ?=
K8S_CHART_PARAMS = \
	--set image.registry=$(IMAGE_REPO) \
	--set image.tag=$(shell . $(RELEASE_SUPPORT); RELEASE_CONTEXT_DIR=$(RELEASE_CONTEXT_DIR) setContextHelper; getVersion)${IMAGE_TAG_EXTRA} \
	--wait --timeout=30m \
	--atomic

DEPLOY_PATH ?= ./dist/
t-v:
	echo ${K8S_CHART_PARAMS}

test-mid-deployment:
	@echo "*****testing mid deployment******"

test-low-deployment:
	@echo "*****testing mid deployment******"

prod-build:
	yarn webpack build \
	--optimization-concatenate-modules --optimization-minimize \
	--mode production \
	--output-clean --output-path $(DEPLOY_PATH)
