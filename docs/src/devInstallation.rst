Installation
~~~~~~~~~~~~

The following notes assume you are at the command prompt for your chosen environment.

1.  Confirm Node and YARN are installed and configured correctly, both the following commands should return the relevant version number.

        > node --version

        > yarn --version

2.  Clone the project from GitHub

        > git clone --recursive git@gitlab.com:ska-telescope/ska-portal.git

3.  Allow yarn to be able to include required SKA libraries

        > npm config set @ska-telescope:registry https://artefact.skao.int/repository/npm-internal/

4.  Install all the necessary project dependencies by running

        > yarn init

5.  Install required SKA libraries 

It is expected that required SKA libraries would have been included at this point,
however if this is found not to be the case, the following command will include them.

        > yarn skao:update

Steps to create a new release
=============================

.. danger:: Note

   This does not currently work in the Windows Shell. 
   Use either Linux, Mac, or Windows WSL.

The following steps and commands is to create a new release for the portal.

1. Create a new branch from ``main`` branch.
2. Run one of ``make bump-major-release``, ``make bump-minor-release``, or ``make bump-patch-release``
3. Update the ``charts/ska-portal/values.yaml`` file, the `image.version` should be updated.
4. Make sure the following files have the new version:
   * ``charts/ska-portal/Chart.yaml``
   * ``package.json``
   * ``.release``
5. Commit all files: `git commit -am "XXX-000 Update version files"`
6. Run ``make git-create-tag``
7. Run ``make git-push-tag``
8. You will then be able to merge that branch back in, and the new release should be created.
