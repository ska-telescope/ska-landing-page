
Welcome to ska-portal's documentation!
======================================

Overview
--------

This is an React application that provides standard tooling and menu structure. It includes standard 
tools for linting, code formatting, and testing which are easily integrated into various 
IDEs. 

It also acts as the modular federation 'parent', accepting a number of other front end application 
(known as modules or children), to be accessed centrally which together form the SKAO suite.

An example of a module is the SKA React Skeleton, which is available as a template for creating additional
modules as needed.  ( See the repository : https://gitlab.com/ska-telescope/templates/ska-react-webapp-skeleton )

About this Guide
----------------

This guide is split into two sections.  The Developer Guide intended for those that wish to enhance the
functionality, whilst the Landing Page is intended for those that wish to use User-Defined cards to enhance
their experience of the portal via the addition of links specific to the User.

.. toctree::
   :maxdepth: 1
   :caption: Developer Guide

   devOverview
   devRequirements
   devInstallation
   devRemotes

.. toctree::
   :maxdepth: 1
   :caption: Landing page

   udcOverview
   udcLayout
   udcJSONContent
   
.. toctree::
   :maxdepth: 1
   :caption: Releases

   releases/Changelog
