Layout
~~~~~~

Below is a representative image of a User Defined Landing page using this feature,
followed by an overview of the various aspects.

.. figure:: /images/aivPortal.png
   :width: 90%
   :alt: AIV Portal representative image

.. note::

   Current implementation includes the suppression of the standard SKAO Portal menu which is usually
   located on the left edge of the application window, below the header.  It is envisaged that this 
   will become a user-defined option in later implementations.  Note that the developer user still
   has the menu available

Page Title
==========

Located to the right of the SKAO logo in the page header.

This is able to be overridden by the user via the appropriate entry in the header section of the JSON

Background image
================

Positioned so as to provide a backdrop to the page content.

This is an optional feature that allows for an image to be linked to a primary filter selection
There is a section in the JSON for these image URLs to be added if required

Header ( not shown )
====================

Positioned at the top of the page content.

If a header entry is provided in the JSON file, then this is displayed. 

Description ( Not shown )
=========================

Positioned at the top of the page content below any displayed header.

If a description entry is provided in the JSON file, then this is displayed.

Filtering options
=================

Located at the top of the page content, below any displayed header or description.
 
User defined JSON file is parsed to generate the list of filtering options and presented for use as needed
The number of filtering options is dictated by the JSON file with a current maximum of 3 levels.
Please refer to the JSON structure on the following page of this guide.

There is also the option to add a info tooltip to the display.  Content should be entered into the
filterInfo string which is part of the header section of the JSON

.. note::

   The contents are also filtered by the selected telescope. 
   This is chosen by the telescope selector that is positioned in the top right of the page header.

Content Cards
=============

Located below the filtering options in the page content.
There is a card displayed for each of the elements defined within the JSON file,
within the restriction of any current active filtering options.  ( e.g Telescope )

.. csv-table:: Card elements
   :header: "Element", "Description"

   "Icon", "Visual representation of the type of page that the card will link to"
   "Title", "Short summary of the card purpose"
   "description", "Paragraph giving further information on the purpose of the card"
   "Click Function", "When clicked the application will attempt to oen the provided URL im a new browser window"
   "Info", "If there is information as part of the JSON, then an info icon is displayed. Hovering over the icon shows the information"

.. note::

   The card elements are all pulled in from the JSON file and so the text content is configured when the JSON is parse.

Grouping ( Not shown )
======================

There is the option to group the cards this is done in 2 steps.

Step 1 : Add the grouping name and optional description into the header section of the JSON as list of objects

Step 2 : Add the group number to the data elements in the JSON file.  Left empty the element will not be in a group