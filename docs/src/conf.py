
# -- Project information -----------------------------------------------------

project = 'ska-portal'
copyright = '2023, SKAO'
author = 'SKAO, (Trevor A Swain)'

# The short X.Y version
version = '0.3.0'
# The full version, including alpha/beta/rc tags
release = '0.3.0'

# -- General configuration ---------------------------------------------------

import sphinx_rtd_theme

def setup(app):
    app.add_css_file('css/custom.css')
    app.add_js_file('js/github.js')

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.githubpages',
]

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

exclude_patterns = []

pygments_style = 'sphinx'

# -- Options for HTML output -------------------------------------------------

html_theme = 'ska_ser_sphinx_theme'

html_context = {
    'theme_logo_only' : True,
    'display_github': True, # Integrate GitHub
    'github_user': 'ska-telescope', # Username
    'github_repo': 'ska-portal', # Repo name
    'github_version': 'master', # Version
    'conf_py_path': '/src/', # Path in the checkout to the docs root
}

html_static_path = ['_static']

# -- Options for HTMLHelp output ---------------------------------------------

htmlhelp_basename = 'ska-portal'

# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
}

latex_documents = [
    (master_doc, 'ska-portal.tex', 'ska-portal Documentation',
     'SKAO, (Trevor A Swain)', 'manual'),
]

# -- Options for manual page output ------------------------------------------

man_pages = [
    (master_doc, 'ska-portal', 'ska-portal Documentation',
     [author], 1)
]

# -- Options for Texinfo output ----------------------------------------------

texinfo_documents = [
    (master_doc, 'ska-portal', 'ska-portal Documentation',
     author, 'ska-portal', 'SKA Portal',
     'Miscellaneous'),
]

# -- Options for Epub output -------------------------------------------------

epub_title = project

epub_exclude_files = ['search.html']

# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

intersphinx_mapping = {"python": ("https://docs.python.org/", None)}


# -- Options for todo extension ----------------------------------------------

todo_include_todos = True
