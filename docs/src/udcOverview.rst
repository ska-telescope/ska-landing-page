Overview
~~~~~~~~

The SKA Portal has the ability to allow the logged in user to tailor the presentation and content 
of their landing page.   This is in the form of cards or a list that provide links to 3rd party software, 
allowing the portal to be the 1st place to look to locate all of your important tools and applications.

The cards are generated from a JSON file into which the user adds information relevant to their own
usage of the cards, including filtering and grouping options as well as an icon to allow for a visual cue.

A handy toggle is also provided that will allow for the information to be displayed in a list structure
instead, which is still affected by the filtering options

.. note::

   This feature is evolving as requirements evolve and the ability to store the contents in a database
   becomes available.   This documentation reflects the current state.

Limitations
===========

Currently this feature is limited to those users with the following roles, but it is expected that 
it will be expanded further as dictated by demand.

- AIV
- EMS 
- ITF
- Developer  ( So that new capabilities do not affect the other users )
