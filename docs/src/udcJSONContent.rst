JSON Structure
~~~~~~~~~~~~~~

The JSON file specific to the user is loaded when the user logs into the application.
Below is a simplified structure with explanation as a guide.

.. code-block:: json

   {
     "header": {
       "header": "",
       "description": "",
       "filterLabels": ["Environment", "Namespace", ""],
       "filterInfo": "",
       "groups": [
          { "name": "Group 1", "description": "Group 1 Description" },
          { "name": "Group 2", "description": "" }
        ],
       "title": "AIV Portal"
     },
     "images": [
       {
         "filter": "itf",
         "backgroundURL": "/images/itf.jpg"
       },
       {
         "filter": "aavs3",
         "backgroundURL": "/images/aavs3.jpg"
       }
     ],
     "data": [
       {
        "telescope": "low",
        "filters": ["itf", "te", ""],
        "group": "",
        "avatar": "Test",
        "details": "Logs, dashboards and statistics for the software involved in testing",
        "info": "",
        "title": "Kibana",
        "URL": "https://k8s.stfc.skao.int/kibana/app/logs/stream?logFilter=(language:kuery,query:%27ska.datacentre:%20%22low-itf%22%20and%20kubernetes.filter2:%20(%22sut%22%20or%20%22te%22)%20and%20ska_severity:%20(%22INFO%22%20or%20%22WARNING%22%20or%20%22ERROR%22)%27)"
       }
     ]
   }

header
======

Selection of title information that is used as part of the page construction.
All of these titles are optional and so the content can be empty as is the case of the
title and description in the example above.

.. csv-table:: Header elements
   :header: "Item", "Description"

   "header", "Title presented in the top-middle of the page content"
   "description", "Description presented in the top-middle of the page content, below the title if present"
   "filterLabels", "Array of filtering titles, with a current maximum of three levels"
   "filterInfo", "Optional string providing details of the filtering"
   "groups", "Array of group names and descriptions"
   "title", "Title that is displayed in the header, to the right of the SKAO logo"

Images
======

Section displaying images associated to specific level 1 filters
If the filter is selected, then the image is displayed as a background

.. csv-table:: Header elements
   :header: "Item", "Description"

   "filter", "Primary filter to which this image is associated"
   "backgroundURL", "URL for the image to be displayed"

data
====

This is an array of entries, each of these represents a card for display within the page content.

.. csv-table:: Data entry elements
   :header: "Item", "Description"

   "telescope", "Indicates that this card is displayed for that telescope. Must contain 'mid' and/or 'low', separated by a comma"
   "filters", "Filtering array of a maximum of 3 strings. For each level multiple filters can be added, separated by a comma"
   "group", "String containing the group number relating to the groups in the header section ( leave blank for no grouping )"
   "avatar", "Used to identify the icon.  If there is no available icon, then the first letter of the entry is displayed"
   "details", "Description of the purpose of the card"
   "info", "Optional information.  If present an icon is displayed.  Hovering over the icon will cause the information to be displayed"
   "title", "Title providing a short identification as to the purpose of the card"
   "url", "The URL that will be attempted to access if the card is clicked"

avatar strings currently able to present an icon
================================================

- KIBANA
- JUYPTER
- JUYPTERHUB
- GRAFANA
- TARANTA
- CALENDAR
- HELP
- TEST
- DOCS
- JIRA
- JAMA
- GIT
- SKAO

