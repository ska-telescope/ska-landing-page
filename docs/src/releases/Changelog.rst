Changelog
=========


Current Development
-------------------

* [Changed] `NAL-1227 <https://jira.skatelescope.org/browse/NAL-1227>`_ 
 
  - Update remote name of the SKA Data Product Dashboard (removed sdp).

v0.3.0
------

* [Changed] `NAL-1071 <https://jira.skatelescope.org/browse/NAL-1071>`_ 
 
  - Updated MS Entra Auth path to obtain user roles from permissions API.

* [Changed] `LOW-828 <https://jira.skatelescope.org/browse/LOW-828>`_ 
 
  - Add cards and configuration for AA0.5.

* [Changed] `NAL-1045 <https://jira.skatelescope.org/browse/NAL-1045>`_ 
 
  - The SKAO Shell were updated in the SKA Portal and DPD to align with the latest auth components.

* [Changed] `NAL-962 <https://jira.skatelescope.org/browse/NAL-962>`_ 
 
  - Update Authentication to align with MS Entra & User role service.

* [Changed] `FUS-151 <https://jira.skatelescope.org/browse/FUS-151>`_ 
 
  - Update JSON file with card links for SKA-Portal.

* [Changed] `STAR-445 <https://jira.skatelescope.org/browse/STAR-445>`_ 
 
  - Documentation and final updates for the Homelinks functionality.

* [Added] `STAR-327 <https://jira.skatelescope.org/browse/STAR-327>`_ 
 
  - Grouping added to AIV work.

* [Changed] `STAR-329 <https://jira.skatelescope.org/browse/STAR-329>`_ 
 
  - Various updated for the AIV/EMS/ITF Portal.

* [Added] `LOW-799 <https://jira.skatelescope.org/browse/LOW-799>`_ 
 
  - Add signal display link and clean up.

* [Changed] `LOW-776 <https://jira.skatelescope.org/browse/LOW-776>`_ 
 
  - Additional git link.

* [Changed] `NAL-1000 <https://jira.skatelescope.org/browse/NAL-1000>`_ 
 
  - Switch to using Poetry for documentation dependencies.

* [Changed] `STAR-328 <https://jira.skatelescope.org/browse/STAR-328>`_ 
 
  - Documentation update.

* [Changed] `LOW-684 <https://jira.skatelescope.org/browse/LOW-684>`_ 
 
  - Add requirements link and fix test cases link.

* [Changed] `NAL-906 <https://jira.skatelescope.org/browse/NAL-906>`_ 
 
  - Update env variables to make use of react-inject-env.

For earlier history, please see the git history.


