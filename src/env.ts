declare global {
  interface Window {
    env: any;
  }
}

type EnvType = {
  REACT_APP_SKA_PORTAL_BASE_URL: string;
  REACT_APP_DOMAIN: string;
  REACT_APP_REMOTE_SDP_DATA_PRODUCT_DASHBOARD_URL: string;
  REACT_APP_REMOTE_SDP_MONITORING_DASHBOARD_URL: string;
  REACT_APP_REMOTE_SIGNAL_DISPLAY_URL: string;
  REACT_APP_REMOTE_PROJECT_TRACKING_TOOL_URL: string;
  REACT_APP_REMOTE_SKA_ANGULAR_WEBAPP_SKELETON_URL: string;
  REACT_APP_REMOTE_REACT_SKELETON_URL: string;
  REACT_APP_USE_LOCAL_DATA: string;
  REACT_APP_MSENTRA_REDIRECT_URI: string;
  REACT_APP_MSENTRA_CLIENT_ID: string;
  REACT_APP_MSENTRA_TENANT_ID: string;
  REACT_APP_ALLOW_MOCK_AUTH: string;
  REACT_APP_PERMISSIONS_API_URI: string;
};
export const env: EnvType = {
  ...process.env,
  ...window.env,
  ...(typeof Cypress !== 'undefined' ? Cypress.env() : {})
};
