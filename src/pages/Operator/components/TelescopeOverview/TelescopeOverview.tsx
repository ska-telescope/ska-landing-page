import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Box, Card, CardContent, Grid, Typography } from '@mui/material';
import SubArrays from './SubArrays/SubArrays';
import { AlertCard } from '@ska-telescope/ska-gui-components';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { PATH } from '@utils/constants';
import { env } from '../../../../env';

const MID = 'SKA MID';

const TelescopeOverview = () => {
  const { t } = useTranslation('operator');
  const navigate = useNavigate();
  const { telescope } = storageObject.useStore();

  const antennaArrayLow = [
    { level: 1, title: 'Errors', filled: false, value: 0, hideValue: false },
    { level: 3, title: 'Warnings', filled: false, value: 3, hideValue: false },
    { level: 5, title: 'Offline', filled: false, value: 2, hideValue: false },
    { level: 0, title: 'aOK', filled: false, value: 65, hideValue: false }
  ];

  const antennaArrayMid = [
    { level: 1, title: 'Errors', filled: false, value: 1, hideValue: false },
    { level: 3, title: 'Warnings', filled: false, value: 0, hideValue: false },
    { level: 5, title: 'Offline', filled: false, value: 0, hideValue: false },
    { level: 0, title: 'OK', filled: false, value: 55, hideValue: false }
  ];

  const subArrayLow = [
    {
      id: 1,
      level: 3,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'High winds affecting results',
      progress: 60
    },
    {
      id: 2,
      level: 0,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'Active',
      progress: 85
    }
  ];

  const subArrayMid = [
    {
      id: 1,
      level: 0,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'Active',
      progress: 60
    },
    {
      id: 2,
      level: 1,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'Positioning failure',
      progress: 85
    },
    {
      id: 3,
      level: 3,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'Positioning failure',
      progress: 85
    },
    {
      id: 4,
      level: 5,
      title: 'Narrow Band 1390 MHz (4)',
      filled: false,
      text: 'Positioning failure',
      progress: 24
    }
  ];

  const getPath = (inValue: string) => {
    return env.REACT_APP_SKA_PORTAL_BASE_URL + inValue;
  };
  const alertClicked = () => navigate(getPath(PATH.ALE));
  const subArrayClicked = () => navigate(getPath(PATH.SUB));

  const cardTitle = () => {
    return t('label.telescopeOverview') + ' (' + telescope?.name + ')';
  };

  const antennaData = () => {
    return telescope?.name === MID ? antennaArrayMid : antennaArrayLow;
  };

  const subArrayData = () => {
    return telescope?.name === MID ? subArrayMid : subArrayLow;
  };

  return (
    <Box m={1}>
      <Card variant="outlined">
        <CardContent>
          <Typography variant="h5" component="div">
            {cardTitle()}
          </Typography>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item xs={7}>
              <SubArrays
                title="label.sub"
                array={subArrayData()}
                clickFunction={subArrayClicked}
              />
            </Grid>
            <Grid item xs={5}>
              <AlertCard
                title={t('label.antennaeFree')}
                array={antennaData()}
                clickFunction={alertClicked}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Box>
  );
};

export default TelescopeOverview;
