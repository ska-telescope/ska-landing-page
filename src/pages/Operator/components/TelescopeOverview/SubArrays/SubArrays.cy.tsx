import React from 'react';
import SubArrays from './SubArrays';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];
const DUMMY_TITLE = 'Sub Arrays';
const DUMMY_ARRAY = [
  {
    id: 1,
    level: 0,
    title: 'Narrow Band 1390 MHz (4)',
    text: 'Active',
    progress: 60
  },
  {
    id: 2,
    level: 1,
    title: 'Narrow Band 1390 MHz (4)',
    text: 'Positioning failure',
    progress: 85
  }
];

describe('<TelescopeOverview />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <SubArrays title={DUMMY_TITLE} array={DUMMY_ARRAY} />
        </ThemeProvider>
      );
    });
  }
});
