import React from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, AlertColor, Box, Grid, Typography } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';
import { Status } from '@ska-telescope/ska-gui-components';

export interface SubArrayProps {
  title: string;
  filled?: boolean;
  array: {
    id: number;
    level: number;
    title: string;
    text: string;
    progress: number;
  }[];
  // eslint-disable-next-line @typescript-eslint/ban-types
  clickFunction?: Function;
}

const STATE_SIZE = 70;
const SEVERITY = ['success', 'error', '', 'warning', '', 'info'];

export function SubArrays({ title, array, clickFunction }: SubArrayProps) {
  const { t } = useTranslation('operator');

  const cardClicked = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    typeof clickFunction != 'undefined' ? clickFunction() : null;
  };

  const setSeverity = () => {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
      if (array[i].level > result) {
        result = array[i].level;
      }
    }
    return SEVERITY[result] as AlertColor;
  };

  const content = (
    id: number,
    level: number,
    title: string,
    progress: number,
    text: string
  ) => {
    return (
      <Grid
        sx={{ minWidth: '20vw' }}
        container
        direction="row"
        justifyContent="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={3}>
          <Status level={level} size={STATE_SIZE} />
        </Grid>
        <Grid item xs={9}>
          {level === 3 && (
            <Typography variant="h6" component="div" sx={{ color: 'black' }}>
              {id} : {t(title)}
            </Typography>
          )}
          {level === 3 && (
            <Typography
              m={1}
              variant="body2"
              component="div"
              sx={{ color: 'black' }}
            >
              {text}
            </Typography>
          )}
          {level !== 3 && (
            <Typography variant="h6" component="div">
              {id} : {t(title)}
            </Typography>
          )}
          {level !== 3 && (
            <Typography m={1} variant="body2" component="div">
              {text}
            </Typography>
          )}
          {level !== 1 && (
            <LinearProgress
              aria-label="progressBar"
              variant="determinate"
              value={progress}
            />
          )}
        </Grid>
      </Grid>
    );
  };

  return (
    <Box key={'subArray' + { title }} m={1}>
      <Alert variant={'outlined'} icon={false} severity={setSeverity()}>
        <Typography variant="h6" component="div">
          {t(title)}
        </Typography>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="flex-start"
        >
          {array.map((arr, index) => {
            return (
              <Grid item key={'subArrayGridItem' + index}>
                <Box key={'subArrayBox' + index} m={1}>
                  <Alert
                    variant={
                      arr.level > 0 && arr.level !== 5 ? 'filled' : 'outlined'
                    }
                    key={'subArrayAlert' + index}
                    icon={false}
                    severity={SEVERITY[arr.level] as AlertColor}
                    onClick={cardClicked}
                  >
                    {content(
                      arr.id,
                      arr.level,
                      arr.title,
                      arr.progress,
                      arr.text
                    )}
                  </Alert>
                </Box>
              </Grid>
            );
          })}
        </Grid>
      </Alert>
    </Box>
  );
}

export default SubArrays;
