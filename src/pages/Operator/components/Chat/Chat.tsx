import React from 'react';
import {
  Box,
  Card,
  CardContent,
  Modal,
  Paper,
  Typography
} from '@mui/material';

const Chat = () => {
  const [isOpen, setIsOpen] = React.useState(false);

  function openChatModal() {
    setIsOpen(true);
  }
  function closeChatModal() {
    setIsOpen(false);
  }

  return (
    <>
      <Card
        onClick={() => {
          openChatModal();
        }}
      >
        <CardContent>
          <Typography variant="h5" component="div">
            CHAT
          </Typography>
        </CardContent>
      </Card>
      <Modal open={isOpen}>
        <Box>
          <Paper>
            <button onClick={closeChatModal}>Close Chat</button>
            <p>
              IRCbot BOT 10:47 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              10:49 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              10:52 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              10:54 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              10:58 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              11:00 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
            <p>
              IRCbot BOT 11:13 AM [irc] From KAROO_MKAT Alarm: new warn alarm -
              Elasticsearch_health_status (cammon_logmon_status_health is
              yellow)
            </p>
          </Paper>
        </Box>
      </Modal>
    </>
  );
};

export default Chat;
