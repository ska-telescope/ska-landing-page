import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Card, CardContent, Grid, Typography } from '@mui/material';
import TelescopeImage from '@components/telescope/TelescopeImage/TelescopeImage';
import TelescopeLocation from '@components/telescope/TelescopeLocation/TelescopeLocation';
import TelescopeWeather from '@components/telescope/TelescopeWeather/TelescopeWeather';
import { AlertCard } from '@ska-telescope/ska-gui-components';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

const MID = 'SKA MID';

const networkMid = [
  { level: 0, title: 'Incoming', filled: false, value: 1, hideValue: true },
  { level: 0, title: 'Outgoing', filled: false, value: 1, hideValue: true }
];

const networkLow = [
  { level: 1, title: 'Incoming', filled: false, value: 1, hideValue: true },
  { level: 3, title: 'Outgoing', filled: false, value: 1, hideValue: true }
];

const SiteOverview = () => {
  const { t } = useTranslation('operator');
  const { telescope } = storageObject.useStore();

  const networkData = () => {
    return telescope?.name === MID ? networkMid : networkLow;
  };

  return (
    <Box m={1}>
      <Card variant="outlined">
        <CardContent>
          <Typography variant="h5" component="div">
            {t('label.siteOverview')}
          </Typography>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item xs={2}>
              <TelescopeImage />
            </Grid>
            <Grid item xs={3}>
              <TelescopeLocation />
            </Grid>
            <Grid item xs={2}>
              <TelescopeWeather />
            </Grid>
            <Grid item xs={5}>
              <AlertCard title={t('label.networks')} array={networkData()} />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Box>
  );
};

export default SiteOverview;
