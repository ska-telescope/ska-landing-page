import React from 'react';
import SiteOverview from './SiteOverview';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<SiteOverview />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <SiteOverview />
        </ThemeProvider>
      );
    });
  }
});
