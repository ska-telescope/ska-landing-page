import React from 'react';
import { Grid, Stack } from '@mui/material';
import SiteOverview from './components/SiteOverview/SiteOverview';
import TelescopeOverview from './components/TelescopeOverview/TelescopeOverview';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

export function Operator() {
  const { telescope } = storageObject.useStore();

  return (
    <Grid container direction="row" justifyContent="space-between">
      <Grid item xs={12}>
        <Stack spacing={2}>
          {telescope && <SiteOverview />}
          {telescope && <TelescopeOverview />}
        </Stack>
      </Grid>
    </Grid>
  );
}

export default Operator;
