import React from 'react';
import Title from './Title';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<Title />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <Title title="CYPRESS TESTING" />
        </ThemeProvider>
      );
    });
  }
});
