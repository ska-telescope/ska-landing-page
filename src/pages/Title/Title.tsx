import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

interface TitleProps {
  title: string;
}

function Title({ ...props }: TitleProps) {
  const { t } = useTranslation('portal');
  const { isDeveloper } = storageObject.useStore();

  return (
    <Grid container m={2} direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'menu.' + props.title}
          variant="h4"
          component="div"
        >
          {t('menu.' + props.title)}
        </Typography>
      </Grid>
      <Grid item xs={4}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'overview.' + props.title}
          variant="h5"
          component="div"
        >
          {t('overview.' + props.title)}
        </Typography>
      </Grid>
      <Grid item xs={2}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        {isDeveloper && (
          <Typography data-testid="developerNotes" variant="h5" component="div">
            Add developer notes here, which will be suppressed for other roles
          </Typography>
        )}
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
}

export default Title;
