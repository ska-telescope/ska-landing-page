import { REMOTE_SKA_ANGULAR_WEBAPP_SKELETON_URL } from '@utils/constants';
import { DynamicLoader } from '@services/dynamicLoader/dynamicLoader';

const NAME = 'skaAngularWebappSkeleton';
const MODULE = './skaAngularWebappSkeleton';

export default function AngularSkeletonWrapper() {
  return DynamicLoader({
    scope: NAME,
    module: MODULE,
    url: REMOTE_SKA_ANGULAR_WEBAPP_SKELETON_URL
  });
}
