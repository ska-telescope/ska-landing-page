import { REMOTE_REACT_SKELETON_URL } from '@utils/constants';
import { DynamicLoader } from '@services/dynamicLoader/dynamicLoader';

const NAME = 'reactSkeleton';
const MODULE = './ReactSkeleton';

export default function ReactSkeleton() {
  return DynamicLoader({
    scope: NAME,
    module: MODULE,
    url: REMOTE_REACT_SKELETON_URL
  });
}
