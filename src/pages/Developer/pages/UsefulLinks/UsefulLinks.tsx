import { Grid, IconButton, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

function UsefulLinks() {
  const { t } = useTranslation('portal');

  const LINKS = [
    {
      title: 'SKAMPI Docs',
      URL: 'https://developer.skao.int/projects/skampi/en/latest/'
    },
    {
      title: 'Developers Portal',
      URL: 'https://developer.skao.int/en/latest/'
    },
    {
      title: 'GIT Repository',
      URL: 'https://gitlab.com/ska-telescope/ska-skampi/'
    }
  ];

  function LinkElement(index: number, title: string, URL: string) {
    return (
      <Grid key={index} item>
        <IconButton
          aria-label={title}
          data-testid={title}
          onClick={() => openLink(URL)}
        >
          {title}
        </IconButton>
      </Grid>
    );
  }

  function openLink(link: string) {
    window.open(link, '_blank');
  }

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignContent="center"
    >
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignContent="center"
      >
        <Grid item>
          <Typography
            m={2}
            data-testid="devLnkTitle"
            variant="h4"
            component="div"
          >
            {t('menu.dev.lnk')}
          </Typography>
        </Grid>
      </Grid>

      <Grid container direction="row" justifyContent="space-evenly">
        {LINKS.map((LINK, index) => LinkElement(index, LINK.title, LINK.URL))}
      </Grid>
    </Grid>
  );
}

export default UsefulLinks;
