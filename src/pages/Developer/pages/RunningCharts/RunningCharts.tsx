import { Grid, Typography } from '@mui/material';
import { TreeView, TreeItem } from '@mui/x-tree-view';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { useTranslation } from 'react-i18next';
import React from 'react';
import {
  GUI_COMPONENTS_VERSION,
  JAVASCRIPT_COMPONENTS_VERSION
} from '@ska-telescope/ska-gui-components';

const k8s_json = {
  chart: 'ska-mid',
  version: '0.8.2',
  sub_charts: [
    { chart: 'ska-tango-base', version: '0.3.5' },
    { chart: 'ska-tango-util', version: '0.3.5' },
    { chart: 'ska-mid-cbf', version: '0.2.9' },
    { chart: 'ska-csp-lmc-mid', version: '0.11.5' },
    { chart: 'ska-sdp', version: '0.9.1' },
    { chart: 'ska-tango-taranta', version: '1.3.1' },
    { chart: 'ska-tango-tangogql', version: '1.3.3' },
    { chart: 'ska-ser-skuid', version: '3.3.3' },
    { chart: 'ska-portal', version: '0.2.2' },
    { chart: 'ska-sim-dishmaster', version: '2.0.2' },
    { chart: 'ska-log-consumer', version: '0.1.6' },
    { chart: 'config-inspector', version: '0.1.0' }
  ]
};
const sub_charts = k8s_json.sub_charts;

function RenderK8sTree() {
  const { t } = useTranslation('portal');

  // TODO: get this from https://k8s.stfc.skao.int/ci-ska-skampi-XXX/config-inspector/ in future

  return (
    <Grid container direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid="devRunTitle"
          variant="h4"
          component="div"
        >
          {t('menu.dev.run')}
        </Typography>
      </Grid>
      <Grid item xs={4}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography data-testid="devRunDesc" variant="h5" component="div">
          {t('overview.dev.run')}
        </Typography>
      </Grid>
      <Grid item xs={2}></Grid>
      <Grid item xs={6}>
        <TreeView
          aria-label="icon expansion"
          data-testid="runningTreeIcon"
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
        >
          <TreeItem
            key="0"
            nodeId={k8s_json.chart}
            label={'Chart: ' + k8s_json.chart + '   ' + k8s_json.version}
          >
            {sub_charts.map(function (chart, i) {
              return (
                <TreeItem
                  key={i}
                  data-testid={'runningTree' + i}
                  nodeId={chart.chart}
                  label={chart.chart + '   ' + chart.version}
                />
              );
            })}
          </TreeItem>
        </TreeView>
      </Grid>
      <Grid item xs={6}>
        <Typography variant="body2" component="div">
          {'JavaScript Components Library ' + JAVASCRIPT_COMPONENTS_VERSION}
        </Typography>
        <Typography variant="body2" component="div">
          {'GUI Components Library ' + GUI_COMPONENTS_VERSION}
        </Typography>
      </Grid>
    </Grid>
  );
}

RenderK8sTree.propTypes = {};

export default RenderK8sTree;
