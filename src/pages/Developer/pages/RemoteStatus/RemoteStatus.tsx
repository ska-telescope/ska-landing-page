import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';
import { Status } from '@ska-telescope/ska-gui-components';
import React from 'react';

function createData(
  name: string,
  port: number,
  menu: string,
  localStatus: number,
  dockerStatus: number,
  notes: string
) {
  return { name, port, menu, localStatus, dockerStatus, notes };
}

const OK = 0;
const ERR = 1;
// const TEST = 2;
const NA = 9;

const rows = [
  createData('Signal Display', 3333, 'Observations', OK, OK, ''),
  createData('SDP Data Products', 8100, 'Resources', OK, OK, ''),
  createData('SDP Monitoring', 8101, 'Resources', OK, OK, ''),
  createData(
    'Angular Skeleton',
    8091,
    'Developer',
    ERR,
    NA,
    'Needs to be re-engineered.  Docker not required'
  ),
  createData('React Skeleton', 8090, 'Developer', OK, NA, 'Docker not required')
];

function RemoteStatus() {
  return (
    <Paper
      elevation={0}
      sx={{ backgroundColor: 'primary.main', height: '90%' }}
    >
      <Grid container direction="row" justifyContent="space-between">
        <Grid item xs={2}></Grid>
        <Grid
          item
          xs={8}
          container
          direction="row"
          alignItems="center"
          justifyContent="center"
        >
          <Typography
            m={2}
            data-testid="implementProgress"
            variant="h5"
            component="div"
          >
            Implementation progress for Dynamic Remotes
          </Typography>
        </Grid>
        <Grid item xs={2}></Grid>

        <Grid item xs={2}></Grid>
        <Grid
          item
          xs={8}
          container
          direction="row"
          alignItems="center"
          justifyContent="center"
        >
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Remote Name</TableCell>
                <TableCell align="right">Port</TableCell>
                <TableCell align="right">Menu</TableCell>
                <TableCell align="right">local Status</TableCell>
                <TableCell align="right">Docker Status</TableCell>
                <TableCell align="right">Notes</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.port}</TableCell>
                  <TableCell align="right">{row.menu}</TableCell>
                  <TableCell align="right">
                    <Status
                      level={row.localStatus}
                      size={15}
                      testId="statusLocalTestId"
                    />
                  </TableCell>
                  <TableCell align="right">
                    <Status
                      level={row.dockerStatus}
                      size={15}
                      testId="statusDockerTestId"
                    />
                  </TableCell>
                  <TableCell align="right">{row.notes}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Grid>
        <Grid item xs={2}></Grid>
      </Grid>
    </Paper>
  );
}

export default RemoteStatus;
