import { Grid, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

function DeveloperHome() {
  const { t } = useTranslation('portal');

  return (
    <Grid container m={1} direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid="devHomeTitle"
          variant="h4"
          component="div"
        >
          {t('menu.dev.hom')}
        </Typography>
      </Grid>
      <Grid item xs={4}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography data-testid="devHomeDesc" variant="h5" component="div">
          {t('overview.dev.hom')}
        </Typography>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
}

export default DeveloperHome;
