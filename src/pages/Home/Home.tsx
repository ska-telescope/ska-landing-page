import React from 'react';
import { Box, Card, CardContent, Grid } from '@mui/material';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import HomeLinks from '@pages/HomeLinks/HomeLinks';
import Operator from '@pages/Operator/Operator';
import TelescopeImage from '@components/telescope/TelescopeImage/TelescopeImage';
import TelescopeLocation from '@components/telescope/TelescopeLocation/TelescopeLocation';
import TelescopeWeather from '@components/telescope/TelescopeWeather/TelescopeWeather';
import { callPermissionsApi } from '@services/PermissionsApi/PermissionsApi';

const AIV_USER = 'aiv';
const EMS_USER = 'ems';
const ITF_USER = 'itf';
const DEV_USER = 'developer';
const LOW_USER = 'low';
const OPR = 'operator';

interface UserPermissions {
  cluster: string;
  role: string;
  service: string;
  telescope: string;
  portal_top_menu_permissions: number[];
}

export function Home() {
  const [userPermissions, setUserPermissions] = React.useState({
    cluster: '',
    role: '',
    service: '',
    telescope: '',
    portal_top_menu_permissions: []
  });
  const { telescope, user } = storageObject.useStore();

  function isOperator() {
    return isUser(OPR);
  }
  function isHomeLinks() {
    return (
      isUser(AIV_USER) ||
      isUser(ITF_USER) ||
      isUser(EMS_USER) ||
      isUser(LOW_USER) ||
      isUser(DEV_USER)
    );
  }

  React.useEffect(() => {
    if (user && user?.username !== 'Guest') {
      const fetchPermissions = async () => {
        try {
          if (user?.token !== '' && user?.token !== undefined) {
            setUserPermissions(await callPermissionsApi(user.token));
          }
        } catch (error) {
          console.error('Error fetching user permissions:', error);
          // Handle errors appropriately (e.g., display error message)
        }
      };
      fetchPermissions();
    }
  }, [user]);

  // Hard coded RBAC to demonstrate what is possible
  function isUser(inValue: string) {
    if (userPermissions && userPermissions?.role) {
      return userPermissions && userPermissions?.role.indexOf(inValue) >= 0;
    }
    return user && user.username && user.username.indexOf(inValue) >= 0;
  }

  return (
    <Box alignItems="center" display="flex" justifyContent="center" m={3}>
      {isHomeLinks() && <HomeLinks />}
      {isOperator() && <Operator />}
      {!isOperator() && !isHomeLinks() && telescope && (
        <Card variant="outlined" sx={{ width: '70vw' }}>
          <CardContent>
            <Grid container direction="row" justifyContent="space-between">
              <Grid display={{ xs: 'none', sm: 'block' }} item sm={12} md={8}>
                <TelescopeImage />
              </Grid>
              <Grid item xs={12} md={4}>
                <Grid
                  container
                  direction="column"
                  justifyContent="space-between"
                >
                  <Grid item>
                    <TelescopeLocation />
                  </Grid>
                  <Grid item>
                    <TelescopeWeather />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      )}
    </Box>
  );
}

export default Home;
