import React from 'react';
import { Box, Grid } from '@mui/material';
import { InfoCard } from '@ska-telescope/ska-gui-components';
import { useTranslation } from 'react-i18next';

interface ErrorProps {
  err: string;
}

function Error({ ...props }: ErrorProps) {
  const { t } = useTranslation();

  return (
    <Box m={1} sx={{ height: '80vh', width: '99%' }}>
      <Grid container alignItems="center" justifyContent="center">
        <InfoCard
          fontSize={25}
          level={1}
          message={t('error.' + props.err)}
          testId="errorPanel"
        />
      </Grid>
    </Box>
  );
}

export default Error;
