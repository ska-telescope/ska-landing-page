export const HEADER = 'HOME';
export const DESCRIPTION = '';

export const LINKS_LOW = [
  {
    details:
      'Logging dashboards, showing the logs and statistics for the software involved in testing',
    id: 'testingLogs',
    title: 'Test Environment & SUT',
    category: 'Logging',
    URL: 'https://k8s.stfc.skao.int/kibana/app/logs/stream?logFilter=(language:kuery,query:%27ska.datacentre:%20%22low-itf%22%20and%20kubernetes.namespace:%20(%22sut%22%20or%20%22test-environment%22)%20and%20ska_severity:%20(%22INFO%22%20or%20%22WARNING%22%20or%20%22ERROR%22)%27)'
  },
  {
    details:
      'Grafana dashboards displaying historical metrics from software and hardware',
    id: 'grafanaTesting',
    title: 'Test Environment & SUT',
    category: 'Grafana',
    URL: 'https://k8s.lowitf.internal.skao.int/grafana2/d/d78d2a8e-4327-4803-9038-d97ddf11c925/itf?orgId=1&refresh=10s&from=now-12h&to=now'
  },
  {
    details:
      'Taranta dashboards for the tango device controlling the Test Environment',
    id: 'tarantaTE',
    title: 'Test Environment',
    category: 'Taranta',
    URL: 'https://k8s.lowitf.internal.skao.int/test-environment/taranta'
  },
  {
    details:
      'Taranta dashboards for the tango device controlling the System Under Test',
    id: 'tarantaSUT',
    title: 'SUT',
    category: 'Taranta',
    URL: 'https://k8s.lowitf.internal.skao.int/SUT/taranta'
  },
  {
    details:
      'A short how-to on getting started with setting up Jupyter notebooks for scripting in the ITF',
    id: 'scripting',
    title: 'Notebook How-to',
    category: 'Documentation',
    URL: 'https://gitlab.com/ska-telescope/aiv/ska-low-itf-scripts/-/blob/main/README.md'
  },
  {
    details: 'Link to the software developer portal',
    id: 'dev',
    category: 'Documentation',
    title: 'SKAO Developer Portal',
    URL: 'https://developer.skao.int/en/latest/'
  }
];

export const LINKS_MID = [
  {
    details: 'SKAMPI Documentation',
    id: 'skampi',
    title: 'SKAMPI Docs',
    category: 'Documentation',
    URL: 'https://developer.skao.int/projects/skampi/en/latest/'
  },
  {
    details: 'Developers Portal.\nShowing line break as an example',
    id: 'dev',
    title: 'Developers Portal',
    category: 'Documentation',
    URL: 'https://developer.skao.int/en/latest/'
  },
  {
    details: 'GIT Repository',
    id: 'git',
    title: 'GIT Repository',
    category: 'Development',

    URL: 'https://gitlab.com/ska-telescope/ska-skampi/'
  },
  {
    details: 'BinderHub',
    id: 'binderHub',
    title: 'BinderHub',
    category: 'Development',
    URL: 'https://k8s.miditf.internal.skao.int/binderhub/'
  }
];
