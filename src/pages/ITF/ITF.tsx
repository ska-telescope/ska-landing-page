import React from 'react';
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Typography
} from '@mui/material';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { TELESCOPE_LOW } from '@ska-telescope/ska-gui-components';
import { DESCRIPTION, HEADER, LINKS_LOW, LINKS_MID } from './ITFData';

function ITF() {
  const { telescope } = storageObject.useStore();

  function isTelescope(inValue: string) {
    return telescope && telescope.code && telescope.code.indexOf(inValue) >= 0;
  }

  function LinkElement(LINK: any) {
    return (
      <Grid item xs={12} sm={6} md={4}>
        <Card
          style={{ backgroundColor: 'primary' }}
          onClick={() => openLink(LINK.URL)}
          variant="outlined"
        >
          <CardHeader
            avatar={<Avatar variant="square">{LINK.category.charAt(0)}</Avatar>}
            title={LINK.category}
          />
          <CardContent>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignContent="center"
            >
              <Grid item>
                <Typography variant="h4" component="div">
                  {LINK.title}
                </Typography>
              </Grid>
            </Grid>

            <Typography variant="body1" component="div">
              {LINK.details.split('\n').map((c: string) => {
                return (
                  <Typography data-testid={c} variant="body1" component="div">
                    {c}
                  </Typography>
                );
              })}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    );
  }

  const getLinks = () =>
    isTelescope(TELESCOPE_LOW.code) ? LINKS_LOW : LINKS_MID;

  function openLink(link: string) {
    window.open(link, '_blank');
  }

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignContent="center"
    >
      {HEADER && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
        >
          <Grid item>
            <h2>{HEADER}</h2>
          </Grid>
        </Grid>
      )}

      {DESCRIPTION && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
        >
          <Grid item>
            <h4>{DESCRIPTION}</h4>
          </Grid>
        </Grid>
      )}

      <Grid container direction="row" justifyContent="space-evenly" spacing={6}>
        {getLinks().map((LINK) => LinkElement(LINK))}
      </Grid>
    </Grid>
  );
}

export default ITF;
