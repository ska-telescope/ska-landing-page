import React from 'react';
import { Card, CardContent, CardHeader, Grid, Typography } from '@mui/material';
import GrafanaButton from '@components/buttons/GrafanaButton/GrafanaButton';
import JupyterButton from '@components/buttons/JuypterButton/JupyterButton';
import KibanaButton from '@components/buttons/KibanaButton/KibanaButton';
import TarantaButton from '@components/buttons/TarantaButton/TarantaButton';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import {
  LOGO_DEFAULT_HEIGHT,
  THEME_DARK
} from '@ska-telescope/ska-gui-components';

export const HEADER = 'LINKS';
export const DESCRIPTION =
  'Below are some common 3rd party applications that are often used. Click on the icon within the cards to open a new browser window containing a landing page for the application.';

export const LINKS = [
  {
    details:
      'Run data analytics at speed and scale for observability, security, and search with Kibana. Powerful analysis on any data from any source, from threat intelligence to search analytics, logs to application monitoring, and much more.',
    id: 'kibana'
  },
  {
    details:
      'A Jupyter Notebook is an open source web application that allows data scientists to create and share documents that include live code, equations, and other multimedia resources.',
    id: 'Juypter'
  },
  {
    details:
      "Grafana is an open-source observability platform for visualizing metrics, logs, and traces collected from your applications. It's a cloud-native solution for quickly assembling data dashboards that let you inspect and analyze your stack.",
    id: 'Grafana'
  },
  {
    details:
      'Taranta (Webjive until v 1.1.5) is a web application that allows a user to create a graphical user interface to interact with Tango devices; the interface may include a variety of charts, numerical indicators, dials, commands that can be used to monitor and to control devices.',
    id: 'Taranta'
  }
];

function LinksHome() {
  const { themeMode } = storageObject.useStore();

  const isDark = () => {
    return themeMode.mode !== THEME_DARK;
  };

  const getIcon = (id: string) => {
    switch (id) {
      case 'kibana':
        return <KibanaButton dark={isDark()} size={LOGO_DEFAULT_HEIGHT} />;
      case 'Juypter':
        return <JupyterButton dark={isDark()} size={LOGO_DEFAULT_HEIGHT} />;
      case 'Grafana':
        return <GrafanaButton dark={isDark()} size={LOGO_DEFAULT_HEIGHT} />;
      default:
        return <TarantaButton dark={isDark()} size={LOGO_DEFAULT_HEIGHT} />;
    }
  };

  function LinkElement(LINK: any) {
    return (
      <Grid key={LINK.id} item xs={12} sm={6} md={4}>
        <Card style={{ backgroundColor: 'primary' }} variant="outlined">
          <CardHeader avatar={getIcon(LINK.id)} title={LINK.id} />
          <CardContent>
            <Typography variant="body1" component="div">
              {LINK.details.split('\n').map((c: string) => {
                return (
                  <Typography
                    key={LINK.description + c}
                    data-testid={c}
                    variant="body1"
                    component="div"
                  >
                    {c}
                  </Typography>
                );
              })}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    );
  }

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignContent="center"
    >
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignContent="center"
      >
        <Typography m={2} variant="h4" component="div">
          {HEADER}
        </Typography>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignContent="center"
      >
        <Typography m={2} variant="subtitle1" component="div">
          {DESCRIPTION}
        </Typography>
      </Grid>

      <Grid container direction="row" justifyContent="space-evenly" spacing={6}>
        {LINKS.map((LINK) => LinkElement(LINK))}
      </Grid>
    </Grid>
  );
}

export default LinksHome;
