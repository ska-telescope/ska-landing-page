import { REMOTE_SIGNAL_DISPLAY_URL } from '@utils/constants';
import { DynamicLoader } from '@services/dynamicLoader/dynamicLoader';

const NAME = 'signalMetrics';
const MODULE = './signalMetrics';

export default function SignalDisplay() {
  return DynamicLoader({
    scope: NAME,
    module: MODULE,
    url: REMOTE_SIGNAL_DISPLAY_URL
  });
}
