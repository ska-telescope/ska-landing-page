import { Grid, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

function ObservationsHome() {
  const { t } = useTranslation('portal');

  return (
    <Grid container direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'menu.obs.hom'}
          variant="h4"
          component="div"
        >
          {t('menu.obs.hom')}
        </Typography>
      </Grid>
      <Grid item xs={4}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'overview.obs.hom'}
          variant="h5"
          component="div"
        >
          {t('overview.obs.hom')}
        </Typography>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
}

export default ObservationsHome;
