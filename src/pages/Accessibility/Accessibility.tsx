import React from 'react';
// import { useTranslation } from 'react-i18next';
import { Box, Grid } from '@mui/material';
import { InfoCard } from '@ska-telescope/ska-gui-components';

interface AccessibilityProps {}

function Accessibility({}: AccessibilityProps) {
  // const { t } = useTranslation('accessibility');
  return (
    <Box m={1} sx={{ height: '80vh', width: '99%' }}>
      <Grid container alignItems="center" justifyContent="center">
        <InfoCard
          testId="accessibilityCardId"
          fontSize={25}
          level={3}
          message="PLACEHOLDER FOR ACCESSIBILITY WORK"
        />
      </Grid>
    </Box>
  );
}

export default Accessibility;
