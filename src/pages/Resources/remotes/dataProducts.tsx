import { REMOTE_SDP_DATA_PRODUCT_DASHBOARD_URL } from '@utils/constants';
import { DynamicLoader } from '@services/dynamicLoader/dynamicLoader';

const NAME = 'dataProductDashboard';
const MODULE = './Dashboard';

export default function DataProducts() {
  return DynamicLoader({
    scope: NAME,
    module: MODULE,
    url: REMOTE_SDP_DATA_PRODUCT_DASHBOARD_URL
  });
}
