import { REMOTE_PROJECT_TRACKING_TOOL_URL } from '@utils/constants';
import { DynamicLoader } from '@services/dynamicLoader/dynamicLoader';

const NAME = 'projectTrackingTool';
const MODULE = './projectTrackingTool';

export default function PPT() {
  return DynamicLoader({
    scope: NAME,
    module: MODULE,
    url: REMOTE_PROJECT_TRACKING_TOOL_URL
  });
}
