import { Grid, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

function ToolingHome() {
  const { t } = useTranslation('portal');

  return (
    <Grid container direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'toolHomeTitle'}
          variant="h4"
          component="div"
        >
          {t('menu.too.hom')}
        </Typography>
      </Grid>
      <Grid item xs={4}></Grid>
      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          m={2}
          data-testid={'toolHomeDesc'}
          variant="h5"
          component="div"
        >
          {t('overview.too.hom')}
        </Typography>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
}

export default ToolingHome;
