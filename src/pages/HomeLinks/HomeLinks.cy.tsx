import React from 'react';
import ITF from './HomeLinks';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ITF />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        // TODO <ThemeProvider theme={theme(theTheme)}>
        // TODO   <CssBaseline />
        <ITF />
        // TODO </ThemeProvider>,
      );
    });
  }
});
