import React from 'react';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Paper,
  Tooltip,
  Typography
} from '@mui/material';
import {
  ButtonToggle,
  DataGrid,
  DropDown,
  Logo,
  THEME_DARK
} from '@ska-telescope/ska-gui-components';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { useTranslation } from 'react-i18next';
import { callPermissionsApi } from '@services/PermissionsApi/PermissionsApi';

import GrafanaIcon from '@components/icons/GrafanaIcon';
import JupyterIcon from '@components/icons/JupyterIcon';
import KibanaIcon from '@components/icons/KibanaIcon';
import TarantaIcon from '@components/icons/TarantaIcon';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import HelpCenterIcon from '@mui/icons-material/HelpCenter';
import SpeedIcon from '@mui/icons-material/Speed';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import JiraIcon from '@components/icons/JiraIcon';
import JamaIcon from '@components/icons/JamaIcon';
import GitIcon from '@components/icons/GitIcon';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

import * as aivData from '../../../public/json/AIVData.json';
import * as devData from '../../../public/json/DeveloperData.json';
import * as emsData from '../../../public/json/EMSData.json';
import * as itfData from '../../../public/json/ITFData.json';
import * as lowData from '../../../public/json/LowData.json';

const LOGO_DEFAULT_SIZE = 50;
const DATA_GRID_HEIGHT = 600;
const DEF_VALUE = 'ALL_VALUES';

const EMPTY_OPTIONS = [
  {
    label: '',
    value: '',
    filter2: [{ label: '', value: '', filter3: [{ label: '', value: '' }] }]
  }
];

interface UserPermissions {
  cluster: string;
  role: string;
  service: string;
  telescope: string;
  portal_top_menu_permissions: number[];
}

function HomeLinks() {
  const { t } = useTranslation('portal');
  const { telescope, themeMode, user } = storageObject.useStore();

  const [display, setDisplay] = React.useState('0');
  const [filter1, setFilter1] = React.useState(DEF_VALUE);
  const [filter2, setFilter2] = React.useState(DEF_VALUE);
  const [filter3, setFilter3] = React.useState(DEF_VALUE);
  const [options, setOptions] = React.useState(EMPTY_OPTIONS);
  const [useHead, setUseHead] = React.useState('');
  const [useDesc, setUseDesc] = React.useState('');
  const [useFilters, setUseFilters] = React.useState(['', '', '']);
  const [filterInfo, setFilterInfo] = React.useState('');

  const [userPermissions, setUserPermissions] = React.useState({
    cluster: '',
    role: '',
    service: '',
    telescope: '',
    portal_top_menu_permissions: []
  });

  const AIV_USER = 'aiv';
  const DEV_USER = 'developer';
  const EMS_USER = 'ems';
  const ITF_USER = 'itf';
  const LOW_USER = 'low';

  let displayGroup = '';

  React.useEffect(() => {
    if (user && user?.username !== 'Guest') {
      const fetchPermissions = async () => {
        try {
          if (user?.token !== '' && user?.token !== undefined) {
            setUserPermissions(await callPermissionsApi(user.token));
          }
        } catch (error) {
          console.error('Error fetching user permissions:', error);
        }
      };
      fetchPermissions();
    }
  }, [user]);

  type Link = { telescope: string; filters: string[] };

  const isDark = () => {
    return themeMode.mode === THEME_DARK;
  };

  const isTelescope = (LINK: Link) => {
    let result = false;
    LINK.telescope.split(',').forEach((e: string) => {
      const t = stripSpaces(e);
      if (t.toUpperCase() === telescope.code.toUpperCase()) {
        result = true;
      }
    });
    return result;
  };

  const setFilter1Value = (e: React.SetStateAction<string>) => {
    setFilter3(DEF_VALUE);
    setFilter2(DEF_VALUE);
    setFilter1(e);
  };

  const setFilter2Value = (e: React.SetStateAction<string>) => {
    setFilter3(DEF_VALUE);
    setFilter2(e);
  };

  const getGroup = (str: string): any => {
    if (!str.length) {
      return { name: '', description: '' };
    }
    const num = Number(str);
    const groups = getHeader().groups;
    if (num > groups?.length - 1) {
      return { name: '', description: '' };
    }
    return groups[num];
  };

  const testGroup = (inGroup: string) => {
    const result = inGroup?.length && inGroup !== displayGroup ? true : false;
    displayGroup = inGroup;
    return result;
  };

  const getIcon = (inValue: string, divider: number) => {
    switch (inValue.toUpperCase()) {
      case 'KIBANA':
        return (
          <KibanaIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />
        );
      case 'JUYPTER':
        return (
          <JupyterIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />
        );
      case 'JUYPTERHUB':
        return (
          <JupyterIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />
        );
      case 'GRAFANA':
        return (
          <GrafanaIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />
        );
      case 'TARANTA':
        return (
          <TarantaIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />
        );
      case 'CALENDAR':
        return (
          <CalendarMonthIcon sx={{ fontSize: LOGO_DEFAULT_SIZE / divider }} />
        );
      case 'HELP':
        return (
          <HelpCenterIcon sx={{ fontSize: LOGO_DEFAULT_SIZE / divider }} />
        );
      case 'TEST':
        return <SpeedIcon sx={{ fontSize: LOGO_DEFAULT_SIZE / divider }} />;
      case 'DOCS':
        return (
          <ReceiptLongIcon sx={{ fontSize: LOGO_DEFAULT_SIZE / divider }} />
        );
      case 'JIRA':
        return <JiraIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />;
      case 'JAMA':
        return <JamaIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />;
      case 'GIT':
        return <GitIcon dark={isDark()} size={LOGO_DEFAULT_SIZE / divider} />;
      case 'SKAO':
        return (
          <Logo dark={isDark()} height={LOGO_DEFAULT_SIZE / divider / 2.5} />
        );
      default:
        return <Avatar variant="square">{inValue.charAt(0)}</Avatar>;
    }
  };

  function openLink(link: string) {
    window.open(link, '_blank');
  }

  const resetFilters = () => {
    setFilter1(DEF_VALUE);
    setFilter2(DEF_VALUE);
    setFilter3(DEF_VALUE);
  };

  function isUser(inValue: string) {
    if (userPermissions && userPermissions?.role) {
      return userPermissions && userPermissions?.role.indexOf(inValue) >= 0;
    }

    return user && user.username && user.username.indexOf(inValue) >= 0;
  }

  const setAllValues = () => {
    const header = getHeader();
    setUseHead(header.header);
    setUseDesc(header.description);
    setUseFilters(header.filterLabels);
    setFilterInfo(header.filterInfo?.length > 0 ? header?.filterInfo : '');
  };

  React.useEffect(() => {
    setAllValues();
  }, []);

  React.useEffect(() => {
    setAllValues();
  }, [telescope]);

  React.useEffect(() => {
    if (useFilters && useFilters[0]?.length) {
      getOptions();
    }
  }, [useFilters]);

  React.useEffect(() => {
    resetFilters();
  }, [options]);

  const getBG = () => {
    let backgroundURL = '';
    getImage().forEach((e: { filter: string; backgroundURL: string }) => {
      if (filter1 === e.filter) {
        backgroundURL = e.backgroundURL;
      }
    });
    return backgroundURL;
  };

  const getData = () => {
    if (isUser(AIV_USER)) {
      return aivData.data;
    } else if (isUser(DEV_USER)) {
      return devData.data;
    } else if (isUser(EMS_USER)) {
      return emsData.data;
    } else if (isUser(ITF_USER)) {
      return itfData.data;
    } else if (isUser(LOW_USER)) {
      return lowData.data;
    }
    return [];
  };

  const getHeader = () => {
    if (isUser(AIV_USER)) {
      return aivData.header;
    } else if (isUser(DEV_USER)) {
      return devData.header;
    } else if (isUser(EMS_USER)) {
      return emsData.header;
    } else if (isUser(ITF_USER)) {
      return itfData.header;
    } else if (isUser(LOW_USER)) {
      return lowData.header;
    }
    return {
      header: '',
      description: '',
      filterLabels: [],
      filterInfo: '',
      groups: [],
      title: ''
    };
  };

  const getImage = () => {
    if (isUser(AIV_USER)) {
      return aivData.images;
    } else if (isUser(DEV_USER)) {
      return devData.images;
    } else if (isUser(EMS_USER)) {
      return emsData.images;
    } else if (isUser(ITF_USER)) {
      return itfData.images;
    } else if (isUser(LOW_USER)) {
      return lowData.images;
    }
    return [];
  };

  const showInfo = (LINK: { info: string | null }) => {
    return LINK.info && LINK.info.length > 0;
  };

  const LinkCard = (LINK: any, key: React.Key) => (
    <Grid
      key={key + '-element'}
      item
      xs={12}
      sm={6}
      md={4}
      onClick={() => openLink(LINK.URL)}
    >
      <Card
        style={{
          backgroundColor: 'primary',
          cursor: 'pointer',
          height: '100%'
        }}
        variant="outlined"
      >
        <CardHeader
          action={
            showInfo(LINK) ? (
              <Tooltip title={LINK.info}>
                <HelpOutlineIcon />
              </Tooltip>
            ) : (
              <></>
            )
          }
          avatar={getIcon(LINK.avatar, 1)}
          component={Box}
          title={LINK.title}
          titleTypographyProps={{
            align: 'center',
            fontWeight: 'bold',
            variant: 'h5'
          }}
        />
        <CardContent>
          <Typography variant="body1" component="div" width="100%">
            {LINK.details.split('\n').map((c: string) => {
              return (
                <Typography
                  key={LINK.description + c}
                  data-testid={c}
                  variant="body1"
                  component="div"
                >
                  {c}
                </Typography>
              );
            })}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );

  const GroupCard = (LINK: any, key: React.Key) => {
    const group = getGroup(LINK.group);
    if (!group || group?.name?.length === 0) {
      return null;
    }
    return (
      <Grid key={key + '-grouping'} item xs={12}>
        <Card
          style={{
            backgroundColor: 'primary',
            cursor: 'pointer',
            height: '100%'
          }}
          variant="outlined"
        >
          <CardHeader
            action={
              group.description?.length > 0 ? (
                <Tooltip title={group.description}>
                  <HelpOutlineIcon />
                </Tooltip>
              ) : (
                <></>
              )
            }
            title={group.name}
            titleTypographyProps={{
              align: 'center',
              fontWeight: 'bold',
              variant: 'h5'
            }}
          />
        </Card>
      </Grid>
    );
  };

  const inFilter = (base: string, values: string) => {
    let result = false;
    values.split(',').forEach((e: string) => {
      if (base.toUpperCase() === stripSpaces(e).toUpperCase()) {
        result = true;
      }
    });
    return result;
  };

  const getLinks = () => {
    let results: any[] = [];
    let i = 1;

    getData().forEach((LINK: Link) => {
      if (isTelescope(LINK)) {
        if (
          !filter1.length ||
          filter1 === DEF_VALUE ||
          inFilter(filter1, LINK.filters[0])
        ) {
          if (
            !filter2.length ||
            filter2 === DEF_VALUE ||
            inFilter(filter2, LINK.filters[1])
          ) {
            if (
              !filter3.length ||
              filter3 === DEF_VALUE ||
              inFilter(filter3, LINK.filters[2])
            ) {
              results.push({
                id: i++,
                link: LINK,
                group: false
              });
            }
          }
        }
      }
    });

    const groupAsc = results.sort((a, b) =>
      a.link.group > b.link.group ? 1 : -1
    );
    const titleAsc = groupAsc.sort((a, b) => {
      if (a.link.group > b.link.group) {
        return 1;
      } else if (a.link.group === b.link.group) {
        return a.link.title > b.link.title ? 1 : -1;
      } else {
        return -1;
      }
    });

    let theOutput: any[] = [];
    titleAsc.forEach((rec) => {
      if (display === '0' && testGroup(rec.link.group)) {
        theOutput.push({ id: rec.id + 'group', group: true, link: rec.link });
      }
      theOutput.push(rec);
    });
    return theOutput;
  };

  const stripSpaces = (e: string) => {
    const tmp = e
      .replace(/ /g, '')
      .split('')
      .filter((ch) => {
        return ch !== ' ';
      });
    return tmp.join('');
  };

  const getOptions = () => {
    let tmp: { filter1: string; filter2: string; filter3: string }[] = [];
    // This pulls all the entries together for the active telescope
    getData().forEach((LINK) => {
      if (isTelescope(LINK) && LINK.filters[0].length) {
        tmp.push({
          filter1: LINK.filters[0],
          filter2: LINK.filters[1],
          filter3: LINK.filters[2]
        });
      }
    });

    if (!useFilters[0]) {
      setOptions([]);
    }

    let tmp2 = useFilters[0]
      ? [
          {
            label: useFilters[0],
            value: DEF_VALUE,
            filter2: useFilters[1]
              ? [
                  {
                    label: useFilters[1],
                    value: DEF_VALUE,
                    filter3: useFilters[2]
                      ? [{ label: useFilters[2], value: DEF_VALUE }]
                      : []
                  }
                ]
              : []
          }
        ]
      : [];

    if (useFilters[0]) {
      tmp.map((e) => {
        const f1Arr = e.filter1.split(',');
        f1Arr.forEach((element) => {
          const f1 = stripSpaces(element);
          let occ1 = tmp2.findIndex((obj) => obj.value === f1);
          if (occ1 < 0) {
            tmp2.push({
              label: f1,
              value: f1,
              filter2: useFilters[1]
                ? [
                    {
                      label: useFilters[1],
                      value: DEF_VALUE,
                      filter3: useFilters[2]
                        ? [{ label: useFilters[2], value: DEF_VALUE }]
                        : []
                    }
                  ]
                : []
            });
          }
        });
      });
    }

    if (useFilters[0] && useFilters[1]) {
      tmp.map((e) => {
        const f1Arr = e.filter1.split(',');
        const f2Arr = e.filter2.split(',');
        f1Arr.forEach((el1) => {
          f2Arr.forEach((el2) => {
            const f1 = stripSpaces(el1);
            const f2 = stripSpaces(el2);
            if (f2) {
              let occ1 = tmp2.findIndex((obj) => obj.value === f1);
              if (occ1 > -1) {
                let occ2 = tmp2[occ1].filter2.findIndex(
                  (obj) => obj.value === f2
                );
                if (occ2 < 0) {
                  const zzz = tmp2[occ1].filter2;
                  zzz.push({
                    label: f2,
                    value: f2,
                    filter3: useFilters[2]
                      ? [{ label: useFilters[2], value: DEF_VALUE }]
                      : []
                  });
                }
              }
            }
          });
        });
      });
    }

    if (useFilters[0] && useFilters[1]) {
      tmp.map((e) => {
        const f1Arr = e.filter1.split(',');
        const f2Arr = e.filter2.split(',');
        const f3Arr = e.filter3.split(',');
        f1Arr.forEach((el1) => {
          f2Arr.forEach((el2) => {
            f3Arr.forEach((el3) => {
              const f1 = stripSpaces(el1);
              const f2 = stripSpaces(el2);
              const f3 = stripSpaces(el3);
              if (f3) {
                let occ1 = tmp2.findIndex((obj) => obj.value === f1);
                if (occ1 > -1) {
                  let occ2 = tmp2[occ1].filter2.findIndex(
                    (obj) => obj.value === f2
                  );
                  if (occ2 > -1) {
                    let occ3 = tmp2[occ1].filter2[occ2].filter3.findIndex(
                      (obj) => obj.value === f3
                    );
                    if (occ3 < 0) {
                      const zzz = tmp2[occ1].filter2[occ2].filter3;
                      zzz.push({ label: f3, value: f3 });
                    }
                  }
                }
              }
            });
          });
        });
      });
    }

    setOptions(tmp2);
  };

  /***********************************************************************/

  const getDisplayOptions = () => {
    return [
      { id: '0', label: t('homeLinks.display.0'), value: '0' },
      { id: '1', label: t('homeLinks.display.1'), value: '1' }
    ];
  };

  const setDisplayValue = (_event: React.MouseEvent<HTMLElement>, e: any) => {
    setDisplay(e);
  };

  /*************************************************************************/

  const columns = [
    {
      field: 'avatar',
      headerName: '',
      width: LOGO_DEFAULT_SIZE,
      renderCell: (e: { row: { link: { avatar: string } } }) =>
        getIcon(e.row.link.avatar, 2)
    },
    {
      field: 'group',
      headerName: t('homeLinks.columns.group'),
      width: 100,
      renderCell: (e: { row: { link: { group: string } } }) => (
        <Typography>{getGroup(e.row.link.group)}</Typography>
      )
    },
    {
      field: 'title',
      headerName: t('homeLinks.columns.title'),
      width: 250,
      renderCell: (e: { row: { link: { title: string } } }) => (
        <Typography>{e.row.link.title}</Typography>
      )
    },
    {
      field: 'details',
      headerName: t('homeLinks.columns.details'),
      flex: 1,
      disableClickEventBubbling: true,
      renderCell: (e: { row: { link: { details: string } } }) => (
        <Typography>{e.row.link.details}</Typography>
      )
    },
    {
      field: 'filters',
      headerName: t('homeLinks.columns.filters'),
      width: 250,
      renderCell: (e: { row: { link: { filters: string[] } } }) => {
        let results = '| ';
        e.row.link.filters.forEach((el) => {
          results = el.length ? results + el + ' | ' : results;
        });
        return <Typography>{results}</Typography>;
      }
    }
  ];
  const extendedColumns = [...columns];

  const clickRow = (e: { row: { link: { URL: string } } }) => {
    openLink(e.row.link.URL);
  };

  /*************************************************************************/

  const getOptions2 = () => {
    const occ1 = options.findIndex((obj) => obj.value === filter1);
    return occ1 === -1 ? [] : options[occ1].filter2;
  };

  const getOptions3 = () => {
    const occ1 = options.findIndex((obj) => obj.value === filter1);
    const occ2 = options[occ1].filter2.findIndex(
      (obj) => obj.value === filter2
    );
    return options[occ1].filter2[occ2].filter3;
  };

  /*************************************************************************/

  return (
    <Box
      sx={{
        backgroundImage: `url(${getBG()})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        height: '100%',
        minHeight: '600px',
        width: '100%'
      }}
    >
      <Grid
        p={1}
        spacing={1}
        container
        direction="column"
        justifyContent="center"
        alignContent="center"
      >
        {useHead.length > 0 && (
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignContent="center"
          >
            <Grid item>
              <h2>{useHead}</h2>
            </Grid>
          </Grid>
        )}

        {useDesc.length > 0 && (
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignContent="center"
          >
            <Grid item>
              <h4>{useDesc}</h4>
            </Grid>
          </Grid>
        )}

        <Grid
          p={1}
          container
          direction="row"
          justifyContent="space-between"
          alignContent="center"
        >
          <Grid item></Grid>
          <Grid item xs={8}>
            {useFilters && useFilters[0]?.length > 0 && (
              <Paper>
                <Grid
                  p={1}
                  container
                  direction="row"
                  justifyContent="space-evenly"
                  alignContent="center"
                >
                  <Grid item xs={1}></Grid>
                  <Grid item xs={1}></Grid>
                  {useFilters && useFilters[0]?.length > 0 && (
                    <Grid item xs={2}>
                      <DropDown
                        label=""
                        options={options}
                        testId="filter1Id"
                        setValue={setFilter1Value}
                        value={filter1}
                      />
                    </Grid>
                  )}
                  {useFilters && useFilters[1]?.length > 0 && (
                    <Grid item xs={2}>
                      <DropDown
                        disabled={getOptions2().length < 2}
                        label=""
                        options={getOptions2()}
                        testId="filter2Id"
                        setValue={setFilter2Value}
                        value={filter2}
                      />
                    </Grid>
                  )}
                  {useFilters && useFilters[2]?.length > 0 && (
                    <Grid item xs={2}>
                      <DropDown
                        disabled={getOptions3().length < 2}
                        label=""
                        options={getOptions3()}
                        testId="filter3Id"
                        setValue={setFilter3}
                        value={filter3}
                      />
                    </Grid>
                  )}
                  <Grid item xs={1}></Grid>
                  <Grid item xs={1}>
                    {useFilters && filterInfo.length > 0 ? (
                      <Tooltip title={filterInfo}>
                        <HelpOutlineIcon />
                      </Tooltip>
                    ) : (
                      <></>
                    )}
                  </Grid>
                </Grid>
              </Paper>
            )}
          </Grid>
          <Grid item>
            <ButtonToggle
              current={display}
              options={getDisplayOptions()}
              setValue={setDisplayValue}
              testId="displaySelectorId"
              toolTip={t('homeLinks.display.toolTip')}
              value={display}
            />
          </Grid>
        </Grid>

        {display === '0' && (
          <Grid
            pl={1}
            container
            direction="row"
            justifyContent="space-evenly"
            alignItems="stretch"
            spacing={1}
          >
            {getLinks().map(
              (LINK: { id: number; group: boolean; link: Link }, key) => {
                return LINK.group
                  ? GroupCard(LINK.link, key)
                  : LinkCard(LINK.link, key);
              }
            )}
          </Grid>
        )}
        {display === '1' && (
          <Paper>
            <DataGrid
              rows={getLinks()}
              columns={extendedColumns}
              height={DATA_GRID_HEIGHT}
              onRowClick={clickRow}
              showBorder={false}
              showMild
              testId="displayOptions"
            />
          </Paper>
        )}
      </Grid>
    </Box>
  );
}

export default HomeLinks;
