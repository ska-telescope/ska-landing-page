import React from 'react';
import Shell from './Shell';
import { CssBaseline, Grid, ThemeProvider, Typography } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<Shell />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <Shell>
            <Grid item>
              <Typography variant="h4">CYPRESS TESTING</Typography>
            </Grid>
          </Shell>
        </ThemeProvider>
      );
    });
  }
});
