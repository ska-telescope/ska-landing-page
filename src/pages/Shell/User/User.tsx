import React from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  Drawer,
  Grid,
  Stack,
  Typography
} from '@mui/material';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import AlarmOnOutlinedIcon from '@mui/icons-material/AlarmOnOutlined';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { IS_DEV } from '@utils/constants';
import SignOutButton from '@components/Auth/SignOutButton';

export interface UserProps {
  open: boolean;
  toggleDrawer: Function;
}

export default function User({ open, toggleDrawer }: UserProps) {
  const { isDeveloper, user } = storageObject.useStore();

  const isNotDeveloper = isDeveloper ? !isDeveloper : true;
  const isEMS = user?.username === 'ems';

  const ShiftButton = () => {
    return (
      <Button
        variant="outlined"
        color="success"
        startIcon={<AlarmOnOutlinedIcon />}
        onClick={toggleDrawer(false)}
      >
        Start Shift
      </Button>
    );
  };

  const Notes = () => {
    return (
      <Card key="notes" variant="outlined">
        <CardContent>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item>
              <Typography variant="h6" component="div">
                Notes
              </Typography>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                color="secondary"
                startIcon={<AddCircleOutlineOutlinedIcon />}
                onClick={toggleDrawer(false)}
              >
                Add
              </Button>
            </Grid>
          </Grid>
          <Stack sx={{ height: '95%' }} spacing={2}>
            <Typography variant="body2" component="div">
              Note 1
            </Typography>
            <Typography variant="body2" component="div">
              Note 2
            </Typography>
          </Stack>
        </CardContent>
      </Card>
    );
  };

  const Issues = () => {
    return (
      <Card key="issues" variant="outlined">
        <CardContent>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item>
              <Typography variant="h6" component="div">
                Issues
              </Typography>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                color="secondary"
                startIcon={<AddCircleOutlineOutlinedIcon />}
                onClick={toggleDrawer(false)}
              >
                Add
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  };

  return (
    <div>
      <Drawer anchor={'right'} open={open} onClose={toggleDrawer(false)}>
        <Box m={1} sx={{ minWidth: '25vw' }}>
          <Stack sx={{ height: '95%' }} spacing={2}>
            <Grid container direction="row" justifyContent="space-evenly">
              <Grid item>
                <SignOutButton />
              </Grid>
              {IS_DEV && isNotDeveloper && !isEMS && (
                <Grid item>
                  <ShiftButton />
                </Grid>
              )}
            </Grid>

            {IS_DEV && !isEMS && <Notes />}
            {IS_DEV && !isEMS && <Issues />}
          </Stack>
        </Box>
      </Drawer>
    </div>
  );
}
