import React from 'react';
import axios from 'axios';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import lstjs from 'local-sidereal-time';
import Tooltip from '@mui/material/Tooltip';

export function TimeWidget() {
  const { t } = useTranslation();

  const API_KEY = 'FODFFGIG9JCW';
  const API_DISABLED = false;
  const FULL_FORMAT = 'LLL';
  const TIME_FORMAT = 'HH:mm';
  const { telescope } = storageObject.useStore();

  const [now, setNow] = React.useState(moment());
  const [timeOffset, setTimeOffset] = React.useState(0);

  React.useEffect(() => {
    const timer = setInterval(() => setNow(moment()), 15000);
    return function cleanup() {
      clearInterval(timer);
    };
  });

  function hours(inValue: number) {
    return inValue / 60 / 60; // Convert to hours from milliseconds
  }

  React.useEffect(() => {
    if (!API_DISABLED && telescope && telescope.position) {
      const { lat, lon } = telescope.position;
      const baseURL = `https://api.timezonedb.com/v2.1/get-time-zone?key=${API_KEY}&format=json&by=position&lat=${lat}&lng=${lon}`;

      axios
        .get(baseURL)
        .then(function (response) {
          setTimeOffset(response.data.gmtOffset);
        })
        .catch(function (error) {
          console.error('Time Error:', error);
          setTimeOffset(0);
        });
    } else {
      setTimeOffset(0);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [telescope]);

  return (
    <Tooltip
      arrow
      placement="top"
      title={
        <p data-testid="long-display">
          {now.local().format(FULL_FORMAT)}
          <br />
          <br />
          {t('timeWidget.utc')}: {now.utcOffset(hours(0)).format(TIME_FORMAT)}
          <br />
          {t('timeWidget.localTime')}: {now.local().format(TIME_FORMAT)}
          <br />
          {t('timeWidget.siteTime')}:{' '}
          {now.utcOffset(hours(timeOffset)).format(TIME_FORMAT)}
          <br />
          <br />
          {t('timeWidget.offset')}: {hours(timeOffset)} {t('timeWidget.hrs')}
          <br />
          {t('timeWidget.siderealTime')}:{' '}
          {lstjs.lstString(new Date(), telescope?.position.lon)}
        </p>
      }
    >
      <div>
        {timeOffset === 0 && <div>{now.local().format(TIME_FORMAT)}</div>}
        {timeOffset !== 0 && (
          <div>
            {now.local().format(TIME_FORMAT)}
            {' / '}
            {now.parseZone().utcOffset(hours(timeOffset)).format(TIME_FORMAT)}
          </div>
        )}
        {telescope && telescope.position && (
          <div>{lstjs.lstString(new Date(), telescope?.position.lon)}</div>
        )}
      </div>
    </Tooltip>
  );
}

export default TimeWidget;
