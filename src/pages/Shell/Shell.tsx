import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { env } from '../../env';
import { LoadUserData } from '@components/Auth/MSEntraAuth/LoadUserData';
import { MsalAuthenticationTemplate } from '@azure/msal-react';
import { InteractionType } from '@azure/msal-browser';

import {
  IconButton,
  Paper,
  Stack,
  Tooltip,
  Typography,
  styled
} from '@mui/material';
// import AccessibilityNewOutlinedIcon from '@mui/icons-material/AccessibilityNewOutlined';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import NotificationsNoneSharpIcon from '@mui/icons-material/NotificationsNoneSharp';
import { storageObject, Telescope } from '@ska-telescope/ska-gui-local-storage';
import { callPermissionsApi } from '@services/PermissionsApi/PermissionsApi';

import {
  CopyrightModal,
  Footer,
  Header,
  SPACER_VERTICAL,
  Spacer
} from '@ska-telescope/ska-gui-components';

import User from './User/User';
import Menu from '@components/menus/Menu/Menu';

import {
  IS_DEV,
  PATH,
  SPACER_FOOTER,
  SPACER_HEADER,
  ALLOW_MOCK_AUTH,
  VERSION
} from '@utils/constants';

import SwitchAuthButton from '@components/Auth/SwitchAuthButton';
import SignInButton from '@components/Auth/SignInButton';
import MockAuthDialogs from '@components/Auth/MockAuth/MockAuthDialogs/MockAuthDialogs';

import * as aivData from '../../../public/json/AIVData.json';
import * as emsData from '../../../public/json/EMSData.json';
import * as itfData from '../../../public/json/ITFData.json';
import * as lowData from '../../../public/json/LowData.json';

export interface LayoutProps {
  children: JSX.Element;
  reqAuth?: boolean;
}

const PREFIX = 'Shell';
const classes = {
  root: `${PREFIX}-root`,
  menuButton: `${PREFIX}-menuButton`
};
const Root = styled('div')(() => ({
  [`&.${classes.root}`]: {
    display: 'flex'
  },
  [`& .${classes.menuButton}`]: {
    marginLeft: '14px'
  }
}));

interface UserPermissions {
  cluster: string;
  role: string;
  service: string;
  telescope: string;
  portal_top_menu_permissions: number[];
}

export default function Shell({ children, reqAuth = true }: LayoutProps) {
  const { t } = useTranslation('portal');
  const [openUser, setOpenUser] = React.useState(false);
  const [showCopyright, setShowCopyright] = React.useState(false);
  const { access, user, application } = storageObject.useStore();
  const username = !user ? '' : user.username;
  const [userPermissions, setUserPermissions] = React.useState({
    cluster: '',
    role: '',
    service: '',
    telescope: '',
    portal_top_menu_permissions: []
  });

  React.useEffect(() => {
    if (user && user?.username !== 'Guest') {
      const fetchPermissions = async () => {
        try {
          if (user?.token !== '' && user?.token !== undefined) {
            setUserPermissions(await callPermissionsApi(user.token));
          }
        } catch (error) {
          console.error('Error fetching user permissions:', error);
        }
      };
      fetchPermissions();
    }
  }, [user]);

  React.useEffect(() => {
    if (username === '') {
      setOpenUser(false);
    }
  }, [username]);

  const toggleDrawer = (newOpen: boolean) => () => {
    setOpenUser(newOpen);
  };

  const TheHeader = () => {
    const { user } = storageObject.useStore();
    const {
      help,
      helpToggle,
      telescope,
      themeMode,
      toggleTheme,
      updateTelescope
    } = storageObject.useStore();

    const telescopeFunction = (e: Telescope) => {
      updateTelescope(e);
    };

    const theStorage = {
      help: help,
      helpToggle: helpToggle,
      telescope: telescope,
      themeMode: themeMode.mode,
      toggleTheme: toggleTheme,
      updateTelescope: telescopeFunction
    };
    const navigate = useNavigate();
    const username = !user ? '' : user.username;
    const [title, setTitle] = React.useState('');

    const skao = t('toolTip.button.skao', { ns: 'transaction' });
    const mode = t('toolTip.button.mode', { ns: 'transaction' });
    const toolTip = { skao: skao, mode: mode };

    React.useEffect(() => {
      if (isUser(AIV_USER) && aivData?.header?.title?.length) {
        setTitle(aivData.header.title);
      } else if (isUser(ITF_USER) && itfData?.header?.title?.length) {
        setTitle(itfData.header.title);
      } else if (isUser(EMS_USER) && emsData?.header?.title?.length) {
        setTitle(emsData.header.title);
      } else if (isUser(LOW_USER) && lowData?.header?.title?.length) {
        setTitle(lowData.header.title);
      } else {
        setTitle(t('application'));
      }
    }, [user]);

    function handleClick() {
      setOpenUser(true);
    }

    const getPath = (inValue: string) => {
      return env.REACT_APP_SKA_PORTAL_BASE_URL + inValue;
    };
    // TODO const accessibilityClicked = () => navigate(getPath(PATH.ACC));
    const alertClicked = () => navigate(getPath(PATH.ALE));
    const chatClicked = () => navigate(getPath(PATH.CHA));

    /* TODO

    const accessibilityButton = () => {
      return (
        <>
          {false && IS_DEV && (
            <Tooltip
              title={t('toolTip.button.accessibility', { ns: 'accessibility' })}
              arrow
            >
              <IconButton
                aria-label={t('label.button.accessibility', {
                  ns: 'accessibility',
                })}
                sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
                onClick={accessibilityClicked}
                color="inherit"
              >
                <AccessibilityNewOutlinedIcon />
              </IconButton>
            </Tooltip>
          )}
        </>
      );
    };
    */

    const chatButton = () => {
      return (
        <>
          {false && IS_DEV && user && (
            <Tooltip title={t('toolTip.button.chat', { ns: 'portal' })} arrow>
              <IconButton
                aria-label={t('label.button.chat', { ns: 'portal' })}
                data-testid="chatIconButton"
                sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
                onClick={chatClicked}
                color="inherit"
              >
                <ChatBubbleOutlineOutlinedIcon />
              </IconButton>
            </Tooltip>
          )}
        </>
      );
    };

    const alertButton = () => {
      return (
        <>
          {false && IS_DEV && user && (
            <Tooltip title={t('toolTip.button.alert', { ns: 'portal' })} arrow>
              <IconButton
                aria-label={t('label.button.alert', { ns: 'portal' })}
                data-testid="alertIconButton"
                sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
                onClick={alertClicked}
                color="inherit"
              >
                <NotificationsNoneSharpIcon />
              </IconButton>
            </Tooltip>
          )}
        </>
      );
    };

    const loggedInButton = () => {
      return (
        <>
          {!username && <SwitchAuthButton />}
          <MsalAuthenticationTemplate interactionType={InteractionType.None}>
            <LoadUserData />
          </MsalAuthenticationTemplate>
          {username && (
            <Tooltip title={t('toolTip.button.user', { ns: 'portal' })} arrow>
              <IconButton
                data-testid="userName"
                role="button"
                aria-label={username}
                sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
                color="inherit"
                onClick={handleClick}
                size="small"
              >
                <Typography variant="h6">{username}</Typography>
              </IconButton>
            </Tooltip>
          )}
          {!username && <SignInButton />}
        </>
      );
    };

    const getDocs = () => {
      if (isHomeLinks()) {
        const headerTip = t('toolTip.button.docs');
        const headerURL = t('toolTip.button.docsURL');
        return { tooltip: headerTip, url: headerURL };
      } else {
        return;
      }
    };

    return (
      <Header
        docs={getDocs()}
        title={title}
        testId="headerId"
        toolTip={toolTip}
        storage={theStorage}
      >
        {/* {accessibilityButton()} */}
        {chatButton()}
        {alertButton()}
        {loggedInButton()}
      </Header>
    );
  };

  const TheFooter = () => {
    return (
      <Footer
        copyrightFunc={setShowCopyright}
        testId="footerId"
        version={VERSION}
        versionTooltip={''}
      />
    );
  };

  const AIV_USER = 'aiv';
  const EMS_USER = 'ems';
  const ITF_USER = 'itf';
  const LOW_USER = 'low';

  function isUser(inValue: string) {
    if (userPermissions && userPermissions?.role) {
      return userPermissions && userPermissions?.role.indexOf(inValue) >= 0;
    }
    return user && user.username && user.username.indexOf(inValue) >= 0;
  }

  function isHomeLinks() {
    return isUser(AIV_USER) || isUser(EMS_USER) || isUser(ITF_USER);
  }

  return (
    <Paper
      elevation={0}
      sx={{ height: '100%', backgroundColor: 'primary.main' }}
    >
      <CopyrightModal copyrightFunc={setShowCopyright} show={showCopyright} />
      <Root className={classes.root}>
        {reqAuth && (
          <>
            {TheHeader()}
            <User open={openUser} toggleDrawer={toggleDrawer} />
            {!isHomeLinks() && (
              <Stack>
                <Spacer size={SPACER_HEADER} axis={SPACER_VERTICAL} />
                {access && access.menu && (
                  <Menu
                    menuItems={
                      user?.username ? access.menu.top : access.menu.def
                    }
                    topMenu={true}
                  />
                )}
              </Stack>
            )}
            <Paper
              elevation={0}
              sx={{
                backgroundColor: 'primary.main',
                width: '100vw',
                minHeight: '100vh'
              }}
            >
              <div>
                <Spacer size={SPACER_HEADER} axis={SPACER_VERTICAL} />
                <main>{children}</main>
                <Spacer size={SPACER_FOOTER} axis={SPACER_VERTICAL} />
              </div>
            </Paper>
          </>
        )}
        {!reqAuth && children}
        {ALLOW_MOCK_AUTH && !application.content1 ? <MockAuthDialogs /> : null}
        {TheFooter()}
      </Root>
    </Paper>
  );
}
