import { env } from '../env';

export const DRAWER_CLOSED = 40;
export const DRAWER_OPEN = 250;

export const SPACER_HEADER = 70;
export const SPACER_FOOTER = 70;
export const SPACER_MENU = 15;

function parseBoolean(str: string): boolean {
  return str ? str.toLowerCase() === 'true' : false;
}

export const VERSION = process.env.REACT_APP_VERSION;
export const IS_DEV = process.env['NODE_ENV'] !== 'production';
export const DOMAIN = env.REACT_APP_DOMAIN;
export const REMOTE_REACT_SKELETON_URL =
  env.REACT_APP_REMOTE_REACT_SKELETON_URL;
export const REMOTE_SDP_DATA_PRODUCT_DASHBOARD_URL =
  env.REACT_APP_REMOTE_SDP_DATA_PRODUCT_DASHBOARD_URL;
export const REMOTE_SIGNAL_DISPLAY_URL =
  env.REACT_APP_REMOTE_SIGNAL_DISPLAY_URL;
export const REMOTE_PROJECT_TRACKING_TOOL_URL =
  env.REACT_APP_REMOTE_PROJECT_TRACKING_TOOL_URL;
export const REMOTE_SKA_ANGULAR_WEBAPP_SKELETON_URL =
  env.REACT_APP_REMOTE_SKA_ANGULAR_WEBAPP_SKELETON_URL;
export const USE_LOCAL_DATA = parseBoolean(env.REACT_APP_USE_LOCAL_DATA);
export const MSENTRA_CLIENT_ID = env.REACT_APP_MSENTRA_CLIENT_ID;
export const MSENTRA_TENANT_ID = env.REACT_APP_MSENTRA_TENANT_ID;
export const MSENTRA_REDIRECT_URI = env.REACT_APP_MSENTRA_REDIRECT_URI;
export const ALLOW_MOCK_AUTH = parseBoolean(env.REACT_APP_ALLOW_MOCK_AUTH);
export const PERMISSIONS_API_URI = env.REACT_APP_PERMISSIONS_API_URI;

export const PATH = {
  ACC: '/accessibility',
  ALE: '/alerts',
  ASK: '/angularSkeleton',
  CHA: '/chat',
  DEV: '/dev',
  HOM: '/',
  LIN: '/login',
  LNK: '/links',
  LOG: '/logistics',
  OBS: '/observations',
  RES: '/resources',
  RSK: '/reactSkeleton',
  DPD: '/dpd',
  SHL: '/shiftLog',
  SIG: '/signalMetrics',
  SUB: '/subArray',
  //
  TOO: '/tooling',
  PTT: '/projectTracking'
};

export const USERS = [
  { label: 'AIV', value: 'aiv' },
  { label: 'Developer', value: 'developer' },
  { label: 'EMS', value: 'ems' },
  { label: 'ITF', value: 'itf' },
  { label: 'LOW', value: 'low' },
  { label: 'Maintainer', value: 'maintainer' },
  { label: 'Operator', value: 'operator' },
  { label: 'Primary Investigator', value: 'pi' },
  { label: 'Co-Investigator', value: 'ci' }
];
