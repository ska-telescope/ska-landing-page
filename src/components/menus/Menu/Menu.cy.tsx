import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import LinkIcon from '@mui/icons-material/Link';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import Menu from './Menu';
import theme from '@services/theme/theme';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { env } from '../../../env';

const THEME = [THEME_DARK, THEME_LIGHT];

const menuItems = [
  {
    title: 'title 1',
    path: 'path 1',
    icon: () => <LinkIcon />
  },
  {
    title: 'title 2',
    path: 'path 2',
    icon: () => <LinkIcon />
  }
];

describe('<Menu />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme + ': TopMenu', () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter basename={env.REACT_APP_SKA_PORTAL_BASE_URL}>
            <Menu menuItems={menuItems} topMenu={true} />
          </BrowserRouter>
        </ThemeProvider>
      );
    });

    it('Theme ' + theTheme + ': SubMenu', () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter basename={env.REACT_APP_SKA_PORTAL_BASE_URL}>
            <Menu menuItems={menuItems} />
          </BrowserRouter>
        </ThemeProvider>
      );
    });
  }
});
