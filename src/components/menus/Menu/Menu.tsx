import * as React from 'react';
import { styled } from '@mui/material/styles';
import { Box, Divider, IconButton, Paper, Tooltip } from '@mui/material';
import KeyboardArrowLeftOutlinedIcon from '@mui/icons-material/KeyboardArrowLeftOutlined';
import KeyboardArrowRightOutlinedIcon from '@mui/icons-material/KeyboardArrowRightOutlined';
import { useTranslation } from 'react-i18next';
import { SPACER_VERTICAL, Spacer } from '@ska-telescope/ska-gui-components';
import { MenuMap } from '@components/menus/MenuMap/MenuMap';
import { SPACER_MENU } from '@utils/constants';

const PREFIX = 'Menu';
const classes = {
  root: `${PREFIX}-root`,
  menuButton: `${PREFIX}-menuButton`
};
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Root = styled('div')(() => ({
  [`&.${classes.root}`]: {
    display: 'flex'
  },
  [`& .${classes.menuButton}`]: {
    marginLeft: '14px'
  }
}));

export interface MenuProps {
  // eslint-disable-next-line @typescript-eslint/ban-types
  menuItems: { title: string; path: string }[];
  topMenu?: boolean;
}

function Menu({ menuItems, topMenu }: MenuProps) {
  const { t } = useTranslation('portal');
  const [open, setOpen] = React.useState(false);

  function handleToggle() {
    setOpen((o) => !o);
  }

  return (
    <Root className={classes.root}>
      <Box m={1}>
        <Paper
          elevation={2}
          sx={{
            backgroundColor: 'secondary.contrastText',
            height: '100%',
            minWidth: '60px'
          }}
        >
          <Tooltip
            title={t('toolTip.button.openClose')}
            placement="right"
            arrow
          >
            <IconButton
              sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 2 }}
              color="inherit"
              data-testid={t('toolTip.button.openClose')}
              aria-label={t('toolTip.button.openClose')}
              onClick={handleToggle}
              className={classes.menuButton}
            >
              {!open && <KeyboardArrowRightOutlinedIcon />}
              {open && <KeyboardArrowLeftOutlinedIcon />}
            </IconButton>
          </Tooltip>
          <Divider orientation="horizontal" variant="middle" />
          {menuItems && (
            <MenuMap open={open} menuItems={menuItems} topMenu={topMenu} />
          )}
          <Spacer size={SPACER_MENU} axis={SPACER_VERTICAL} />
        </Paper>
      </Box>
    </Root>
  );
}

export default Menu;
