import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Tooltip
} from '@mui/material';

// MENU ICONS
import BuildCircleOutlinedIcon from '@mui/icons-material/BuildCircleOutlined';
import EditCalendarOutlinedIcon from '@mui/icons-material/EditCalendarOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import ViewArrayOutlinedIcon from '@mui/icons-material/ViewArrayOutlined';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import HistoryOutlinedIcon from '@mui/icons-material/HistoryOutlined';
import CodeIcon from '@mui/icons-material/Code';
import DnsIcon from '@mui/icons-material/Dns';
import SettingsInputCompositeOutlinedIcon from '@mui/icons-material/SettingsInputCompositeOutlined';
import ImportantDevicesIcon from '@mui/icons-material/ImportantDevices';
import SchemaIcon from '@mui/icons-material/Schema';
import LinkIcon from '@mui/icons-material/Link';
import CloudSyncOutlinedIcon from '@mui/icons-material/CloudSyncOutlined';
import Inventory2OutlinedIcon from '@mui/icons-material/Inventory2Outlined';
import LegendToggleOutlinedIcon from '@mui/icons-material/LegendToggleOutlined';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
//
import AngularIcon from '@pages/Developer/assets/AngularLogo';
import ReactIcon from '@pages/Developer/assets/ReactLogo';

import { useTranslation } from 'react-i18next';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { env } from '../../../env';

const COL_01 = 'white';
const COL_02 = 'black';

export interface MenuMapProps {
  open: boolean;
  menuItems: {
    path: string;
    title: string;
  }[];
  topMenu?: boolean;
}

export function MenuMap({ open, menuItems, topMenu }: MenuMapProps) {
  const { t } = useTranslation('portal');
  const { darkMode } = storageObject.useStore();
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  function fillColor(): string {
    return darkMode ? COL_01 : COL_02;
  }

  const getIcon = (inValue: string) => {
    switch (inValue) {
      case 'menu.hom':
        return <HomeOutlinedIcon />;
      case 'menu.too.title':
        return <BuildCircleOutlinedIcon />;
      case 'menu.sub':
        return <ViewArrayOutlinedIcon />;
      case 'menu.obs.title':
        return <VisibilityOutlinedIcon />;
      case 'menu.res.title':
        return <SettingsInputCompositeOutlinedIcon />;
      case 'menu.log':
        return <DnsIcon />;
      case 'menu.shl':
        return <HistoryOutlinedIcon />;

      case 'menu.dev.hom':
        return <ImportantDevicesIcon />;
      case 'menu.dev.run':
        return <SchemaIcon />;
      case 'menu.lnk.hom':
        return <OpenInNewIcon />;
      case 'menu.dev.lnk':
        return <LinkIcon />;
      case 'menu.dev.ask':
        return <AngularIcon />;
      case 'menu.dev.rsk':
        return <ReactIcon />;
      case 'menu.dev.rem':
        return <CloudSyncOutlinedIcon />;

      case 'menu.obs.hom':
        return <HomeOutlinedIcon />;
      case 'menu.obs.sig':
        return <VisibilityOutlinedIcon />;
      case 'menu.res.hom':
        return <HomeOutlinedIcon />;
      case 'menu.res.dpd':
        return <Inventory2OutlinedIcon />;
      case 'menu.res.mon':
        return <LegendToggleOutlinedIcon />;

      case 'menu.too.hom':
        return <HomeOutlinedIcon />;
      case 'menu.too.ptt':
        return <EditCalendarOutlinedIcon />;

      default:
        return <CodeIcon />;
    }
  };

  const getPath = (inValue: string) => {
    return topMenu ? env.REACT_APP_SKA_PORTAL_BASE_URL + inValue : inValue;
  };

  function navigateTo(path: string): string {
    return topMenu ? getPath(path) : `.${getPath(path)}`;
  }

  const handleListItemClick = (
    _event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ) => {
    setSelectedIndex(index);
  };

  return (
    <>
      {menuItems.map((route, index) => {
        const isSelected = () => selectedIndex === index;

        return (
          <NavLink key={index} to={navigateTo(route.path)}>
            <ListItemButton
              tabIndex={-1}
              onClick={(event) => handleListItemClick(event, index)}
              selected={isSelected()}
              sx={{
                '&.Mui-selected': {
                  backgroundColor: 'primary.dark'
                },
                '&.Mui-focusVisible': {
                  backgroundColor: 'primary.dark'
                },
                ':hover': {
                  backgroundColor: 'primary.main'
                },
                'svg path': { fill: fillColor }
              }}
            >
              <Tooltip title={t(route.title)} placement="right" arrow>
                <ListItemIcon
                  role="button"
                  sx={{
                    minWidth: '30px',
                    padding: '5px',
                    'svg path': { fill: fillColor }
                  }}
                >
                  {getIcon(route.title)}
                </ListItemIcon>
              </Tooltip>
              {open && (
                <ListItemText
                  aria-hidden="true"
                  primaryTypographyProps={{ color: 'secondary.main' }}
                >
                  {t(route.title)}
                </ListItemText>
              )}
            </ListItemButton>
          </NavLink>
        );
      })}
    </>
  );
}
