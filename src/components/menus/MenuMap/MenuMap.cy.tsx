import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import { MenuMap } from './MenuMap';
import LinkIcon from '@mui/icons-material/Link';
import theme from '@services/theme/theme';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { env } from '../../../env';

const THEME = [THEME_DARK, THEME_LIGHT];

const open = false;
const topMenu = false;

const menuItemsFunction = () => {
  return [
    {
      title: 'title 1',
      path: 'path 1',
      icon: () => <LinkIcon />
    },
    {
      title: 'title 2',
      path: 'path 2',
      icon: () => <LinkIcon />
    }
  ];
};

describe('<MenuMap/>', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme + ': TopMenu', () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);

      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter basename={env.REACT_APP_SKA_PORTAL_BASE_URL}>
            <MenuMap
              open={open}
              menuItems={menuItemsFunction()}
              topMenu={topMenu}
            />
          </BrowserRouter>
        </ThemeProvider>
      );
    });
  }
});
