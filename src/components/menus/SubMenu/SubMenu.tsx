import * as React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Box, Grid } from '@mui/material';
import Menu from '@components/menus/Menu/Menu';

import AngularSkeletonWrapper from '@pages/Developer/remotes/angularSkeletonWrapper/angularSkeletonWrapper';
import ReactSkeleton from '@pages/Developer/remotes/reactSkeleton/reactSkeleton';
import UsefulLinks from '@pages/Developer/pages/UsefulLinks/UsefulLinks';
import RenderK8sTree from '@pages/Developer/pages/RunningCharts/RunningCharts';
import DeveloperPage from '@pages/Developer/pages/DeveloperHome/DeveloperHome';
import RemoteStatus from '@pages/Developer/pages/RemoteStatus/RemoteStatus';
import ObservationsHome from '@pages/Observations/ObservationsHome';
import ToolingHome from '@pages/Tooling/ToolingHome';
import LinksHome from '@pages/Links/LinksHome';
import ProjectTrackingTool from '@pages/Tooling/remotes/ProjectTrackingTool/ProjectTrackingTool';
import SignalDisplay from '@pages/Observations/remotes/SignalDisplay/SignalDisplay';
import ResourcesHome from '@pages/Resources/pages/ResourcesHome/ResourcesHome';
import DataProducts from '@pages/Resources/remotes/dataProducts';
import Error from '@pages/Error/Error';
import Loader from '@components/loader/Loader';

export interface SubMenuProps {
  pages: { title: string; path: string }[] | undefined;
  topMenu?: boolean;
}

export default function SubMenu({ pages, topMenu }: SubMenuProps) {
  const getElement = (inValue: string) => {
    switch (inValue) {
      case 'menu.dev.hom':
        return <DeveloperPage />;
      case 'menu.dev.run':
        return <RenderK8sTree />;
      case 'menu.dev.lnk':
        return <UsefulLinks />;
      case 'menu.dev.ask':
        return <AngularSkeletonWrapper />;
      case 'menu.dev.rsk':
        return <ReactSkeleton />;
      case 'menu.dev.rem':
        return <RemoteStatus />;

      case 'menu.obs.hom':
        return <ObservationsHome />;
      case 'menu.obs.sig':
        return <SignalDisplay />;

      case 'menu.res.hom':
        return <ResourcesHome />;
      case 'menu.res.dpd':
        return <DataProducts />;

      case 'menu.lnk.hom':
        return <LinksHome />;

      case 'menu.too.hom':
        return <ToolingHome />;
      case 'menu.too.ptt':
        return <ProjectTrackingTool />;

      default:
        return <Error err={'404'} />;
    }
  };

  return (
    <React.Suspense fallback={<Loader />}>
      <Box sx={{ height: '50%', width: '100%' }}>
        <Grid container direction="row" justifyContent="flex-start" spacing={2}>
          <Grid item xs={'auto'}>
            {pages && <Menu menuItems={pages} topMenu={topMenu} />}
          </Grid>
          <Grid item xs={10}>
            {pages && (
              <Routes>
                {pages.map((page, index) => {
                  return (
                    <Route
                      key={index}
                      path={page.path}
                      element={getElement(page.title)}
                    />
                  );
                })}
              </Routes>
            )}
          </Grid>
        </Grid>
      </Box>
    </React.Suspense>
  );
}
