import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import LinkIcon from '@mui/icons-material/Link';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import SubMenu from './SubMenu';
import theme from '@services/theme/theme';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';
import { env } from '../../../env';

const THEME = [THEME_DARK, THEME_LIGHT];

const SubMenuItems = [
  {
    title: 'title 1',
    path: 'path 1',
    icon: () => <LinkIcon />
  },
  {
    title: 'title 2',
    path: 'path 2',
    icon: () => <LinkIcon />
  }
];

describe('<SubMenu />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme + ': TopSubMenu', () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter basename={env.REACT_APP_SKA_PORTAL_BASE_URL}>
            <SubMenu pages={SubMenuItems} topMenu />
          </BrowserRouter>
        </ThemeProvider>
      );
    });

    it('Theme ' + theTheme + ': SubSubMenu', () => {
      // cy.stub(storageObject, 'useStore').resolves(mockTelescope);
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter basename={env.REACT_APP_SKA_PORTAL_BASE_URL}>
            <SubMenu pages={SubMenuItems} />
          </BrowserRouter>
        </ThemeProvider>
      );
    });
  }
});
