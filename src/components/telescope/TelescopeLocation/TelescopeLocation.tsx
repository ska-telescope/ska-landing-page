import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Card, CardContent, Typography } from '@mui/material';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

const TelescopeLocation = () => {
  const { t } = useTranslation('portal');
  const { telescope } = storageObject.useStore();

  const lat = () => {
    return t('label.latitude') + ' : ' + telescope?.position?.lat;
  };
  const lon = () => {
    return t('label.longitude') + ' : ' + telescope?.position?.lon;
  };

  return (
    <Box m={1}>
      <Card variant="outlined" sx={{ width: '95%' }}>
        <CardContent>
          <Typography variant="h5" component="div">
            {telescope?.location}
          </Typography>
          <Typography variant="body2" component="div">
            {lat()}
          </Typography>
          <Typography variant="body2" component="div">
            {lon()}
          </Typography>
        </CardContent>
      </Card>
    </Box>
  );
};

export default TelescopeLocation;
