import React from 'react';
import { Box } from '@mui/material';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

const TelescopeImage = () => {
  const { telescope } = storageObject.useStore();

  return (
    <Box
      component="img"
      id={telescope?.name + 'Id'}
      alt={telescope?.name}
      src={telescope?.image}
      sx={{ width: '80%' }}
    />
  );
};

export default TelescopeImage;
