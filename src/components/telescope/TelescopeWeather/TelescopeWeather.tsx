import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Card, CardContent, Typography } from '@mui/material';
import { WeatherWidget } from '@components/weather/WeatherWidget/WeatherWidget';
const TelescopeWeather = () => {
  const { t } = useTranslation('portal');

  return (
    <Box m={1}>
      <Card variant="outlined" sx={{ width: '95%' }}>
        <CardContent>
          <Typography variant="h6" component="div">
            {t('label.weather')}
          </Typography>
          <WeatherWidget icon />
        </CardContent>
      </Card>
    </Box>
  );
};

export default TelescopeWeather;
