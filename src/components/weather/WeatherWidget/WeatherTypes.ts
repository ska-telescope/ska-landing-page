export type Weather = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

export type MainWeather = {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
};

export type Wind = {
  speed: number;
  deg: number;
  gust: number;
};

export interface WeatherData {
  coord: {
    lat: number;
    lon: number;
  };
  weather: Weather[];
  base: string;
  main: MainWeather;
  visibility: number;
  wind: Wind;
}

export interface WeatherState {
  weather: WeatherData | 'NA';
}
