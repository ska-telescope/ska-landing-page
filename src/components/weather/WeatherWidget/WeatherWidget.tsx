import React from 'react';
import axios from 'axios';
import { Box, Grid, Tooltip } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

import { WeatherData } from './WeatherTypes';
import WeatherIcon from '@components/weather/WeatherIcon/WeatherIcon';

export type WeatherWidgetProps = {
  icon?: boolean;
};

const WeatherWidget = ({ icon }: WeatherWidgetProps) => {
  const { t } = useTranslation();
  const INTERVAL = 1000 * 60 * 15; // Every 15 minutes

  const API_KEY = '86cd7581ca1f09110892a5f5f882ffff';

  const [theWeather, setTheWeather] = React.useState(null);
  const [axiosError, setAxiosError] = React.useState(false);
  const [pingWeather, setPingWeather] = React.useState(false);
  const { telescope } = storageObject.useStore();

  React.useEffect(() => {
    const API_DISABLED = false;

    function getTheWeather() {
      if (!API_DISABLED && telescope && telescope.position) {
        const { lat, lon } = telescope.position;
        const baseURL = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`;

        axios
          .get(baseURL)
          .then(function (response) {
            setTheWeather(response.data);
            setAxiosError(false);
          })
          .catch(function (error) {
            console.error('Weather Error:', error);
            setTheWeather(null);
            setAxiosError(true);
          });
      } else {
        setTheWeather(null);
        setAxiosError(false);
      }
    }

    getTheWeather();
  }, [telescope, pingWeather]);

  const checkWeather = () => {
    setPingWeather(!pingWeather);
  };

  setInterval(checkWeather, INTERVAL);

  let weatherBit = null;
  let toolTip = null;
  if (theWeather !== null) {
    const weather: WeatherData = theWeather;
    const temp = Math.round(weather.main.temp);
    const description = weather.weather[0].description;
    weatherBit = (
      <Grid container direction="row" justifyContent="space-between">
        {icon && (
          <Grid
            item
            xs={3}
            container
            direction="row"
            alignItems="center"
            justifyContent="center"
          >
            <Box
              sx={{
                borderRadius: '25px',
                backgroundColor: 'primary.dark',
                opacity: '50%',
                padding: 0
              }}
            >
              <WeatherIcon icon={weather.weather[0].icon} size="small" />
            </Box>
          </Grid>
        )}
        <Grid item xs={1}></Grid>
        <Grid item xs={icon ? 8 : 12}>
          <div>{temp}&deg;C</div>
          <div>{description}</div>
        </Grid>
      </Grid>
    );
    toolTip = (
      <p>
        {weather.main.temp}&deg;C
        <br />
        {weather.wind.speed} m/s <br />
        {weather.main.humidity} %<br />
        {weather.main.pressure} hPa
      </p>
    );
  }
  return (
    <>
      {axiosError && <div>{t('error.weather')}</div>}
      {theWeather && (
        <Tooltip arrow placement="top" title={<p>{toolTip}</p>}>
          <>{weatherBit && <div>{weatherBit}</div>}</>
        </Tooltip>
      )}
    </>
  );
};

export default WeatherWidget;

export { WeatherWidget };
