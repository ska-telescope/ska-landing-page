import React from 'react';

type IconSize = 'extra small' | 'small' | 'large' | 'extra large';
type IconSizeSlug = '' | '@2x' | '@4x';
type IconDim = 25 | 50 | 100 | 200;
type IconSizeOut = {
  slug: IconSizeSlug;
  dim: IconDim;
};

interface WeatherIconProperties {
  icon: string;
  size?: IconSize;
}

const size_map = new Map<IconSize, IconSizeOut>([
  ['extra small', { slug: '', dim: 25 }],
  ['small', { slug: '', dim: 50 }],
  ['large', { slug: '@2x', dim: 100 }],
  ['extra large', { slug: '@4x', dim: 200 }]
]);

const WeatherIcon = ({ icon, size = 'small' }: WeatherIconProperties) => {
  const icon_size = size_map.get(size);
  const icon_src = `https://openweathermap.org/img/wn/${icon}${icon_size?.slug}.png`;
  return (
    <img src={icon_src} alt="" width={icon_size?.dim} height={icon_size?.dim} />
  );
};

export default WeatherIcon;
