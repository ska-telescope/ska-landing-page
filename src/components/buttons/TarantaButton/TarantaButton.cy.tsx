import React from 'react';
import TarantaButton from './TarantaButton';
import { CssBaseline, ThemeProvider } from '@mui/material';
import {
  LOGO_DEFAULT_HEIGHT,
  THEME_DARK,
  THEME_LIGHT
} from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];
describe('<TarantaButton />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <TarantaButton
            dark={theTheme === THEME_DARK}
            size={LOGO_DEFAULT_HEIGHT}
          />
        </ThemeProvider>
      );
      // TODO cy.get(`[data-testid="taranta"]`).should('be.visible').click();
    });
  }
});
