import React from 'react';
import { IconButton, Tooltip } from '@mui/material';
import { External } from '@services/external/external';
import TarantaIcon from '../../icons/TarantaIcon';

interface ButtonProps {
  ariaDescription?: string;
  dark?: boolean;
  size: number;
}

function TarantaButton({ ...props }: ButtonProps) {
  const external = External('taranta');

  function openLink() {
    window.open(external?.url, '_blank');
  }

  return (
    <Tooltip title={external?.label} arrow>
      <IconButton
        aria-label={external?.label}
        sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
        onClick={() => openLink()}
        color="inherit"
      >
        <TarantaIcon
          ariaDescription={props.ariaDescription}
          dark={props.dark}
          size={props.size}
        />
      </IconButton>
    </Tooltip>
  );
}

TarantaButton.defaultProps = {
  ariaDescription:
    'CLicking on the logo will open a website page in a new browser window',
  size: 50
};

export default TarantaButton;
