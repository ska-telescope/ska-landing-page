import React from 'react';
import KibanaButton from './KibanaButton';
import { CssBaseline, ThemeProvider } from '@mui/material';
import {
  LOGO_DEFAULT_HEIGHT,
  THEME_DARK,
  THEME_LIGHT
} from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<KibanaButton />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <KibanaButton
            dark={theTheme === THEME_DARK}
            size={LOGO_DEFAULT_HEIGHT}
          />
        </ThemeProvider>
      );
      // TODO cy.get(`[data-testid="kibana"]`).should('be.visible').click();
    });
  }
});
