import React from 'react';
import { IconButton, Tooltip } from '@mui/material';
import { External } from '@services/external/external';
import KibanaIcon from '../../icons/KibanaIcon';

const COL_01 = '#E8478B';
const COL_02 = '#40BEB0';
const COL_03 = 'white';
const COL_04 = '#231F20';
const COL_05 = 'black';

interface ButtonProps {
  ariaDescription?: string;
  dark?: boolean;
  link?: string;
  size: number;
}

function KibanaButton({ ...props }: ButtonProps) {
  const external = External('kibana');

  function openLink() {
    window.open(external?.url, '_blank');
  }

  return (
    <Tooltip title={external?.label} arrow>
      <IconButton
        aria-label={external?.label}
        sx={{ '&:hover': { backgroundColor: 'primary.dark' }, ml: 1 }}
        onClick={() => openLink()}
        color="inherit"
      >
        <KibanaIcon
          ariaDescription={props.ariaDescription}
          dark={props.dark}
          size={props.size}
        />
      </IconButton>
    </Tooltip>
  );
}

KibanaButton.defaultProps = {
  ariaDescription:
    'CLicking on the logo will open a website page in a new browser window',
  size: 50
};

export default KibanaButton;
