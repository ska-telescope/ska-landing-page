import React from 'react';
import GrafanaButton from './GrafanaButton';
import { CssBaseline, ThemeProvider } from '@mui/material';
import {
  LOGO_DEFAULT_HEIGHT,
  THEME_DARK,
  THEME_LIGHT
} from '@ska-telescope/ska-gui-components';
import theme from '@services/theme/theme';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<GrafanaButton />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <GrafanaButton
            dark={theTheme === THEME_DARK}
            size={LOGO_DEFAULT_HEIGHT}
          />
        </ThemeProvider>
      );
      // TODO cy.get(`[data-testid="grafana"]`).should('be.visible').click();
    });
  }
});
