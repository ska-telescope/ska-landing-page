import React from 'react';
import { External } from '@services/external/external';

const COL_01 = '#E8478B';
const COL_02 = '#40BEB0';
const COL_03 = 'white';
const COL_04 = '#231F20';
const COL_05 = 'black';

interface ButtonProps {
  ariaDescription?: string;
  dark?: boolean;
  link?: string;
  size: number;
}

function KibanaIcon({ ...props }: ButtonProps) {
  const external = External('kibana');

  const usedColour = (inValue: string) => {
    if (!props.dark) {
      return inValue === COL_03 ? COL_05 : COL_03;
    } else {
      return inValue;
    }
  };

  return (
    <svg
      aria-label={external?.label}
      aria-describedby="svg-title svg-description"
      data-testid="kibana"
      height={props.size}
      preserveAspectRatio="xMinYMin meet"
      role="img"
      viewBox="0 0 200 200"
      width={props.size}
      xmlns="http://www.w3.org/s2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <path
        fill={usedColour(COL_01)}
        d="M2.7,69.1c24.5,0,47.6,6.3,67.7,17.2L142,0H0v171.1v0V69.1C0.9,69.1,1.8,69.1,2.7,69.1z"
        role="presentation"
      />

      <path
        fill={usedColour(COL_02)}
        d="M70.4,86.3L0,171.1v13.5h141.9C133.9,142.1,107.1,106.3,70.4,86.3z"
        role="presentation"
      />

      <path
        fill={usedColour(COL_03)}
        d="M70.4,86.3L0,171.1v13.5h25.3l68.3-82.4c0,0-4.5-3.8-11-8.3C77.7,90.3,70.4,86.3,70.4,86.3z"
        role="presentation"
      />

      <path
        fill={usedColour(COL_04)}
        d="M2.7,69.1c-0.9,0-1.8,0.1-2.7,0.1v101.9l70.4-84.7C50.3,75.3,27.2,69.1,2.7,69.1z"
        role="presentation"
      />
    </svg>
  );
}

KibanaIcon.defaultProps = {
  ariaDescription:
    'CLicking on the logo will open a website page in a new browser window',
  size: 50
};

export default KibanaIcon;
