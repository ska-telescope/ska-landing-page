import React from 'react';
import { External } from '@services/external/external';

interface ButtonProps {
  ariaDescription?: string;
  dark?: boolean;
  size: number;
}

function JiraIcon({ ...props }: ButtonProps) {
  const external = External('jira');

  const usedColour = () => (props && props.dark ? 'white' : 'black');

  return (
    <svg
      aria-label={external?.label}
      aria-describedby="svg-title svg-description"
      width={props.size}
      height={props.size}
      viewBox="0 0 48 48"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>jira</title>
      <g id="Layer_2" data-name="Layer 2">
        <g id="invisible_box" data-name="invisible box">
          <rect width="48" height="48" fill="none" />
        </g>
        <g id="Q3_icons" data-name="Q3 icons">
          <g>
            <path
              fill={usedColour()}
              d="M44.2,2H23a9.6,9.6,0,0,0,9.5,9.6h3.9v3.7A9.6,9.6,0,0,0,46,24.9V3.8A1.8,1.8,0,0,0,44.2,2Z"
            />
            <path
              fill={usedColour()}
              d="M33.7,12.6H12.5A9.6,9.6,0,0,0,22,22.1h4v3.8a9.4,9.4,0,0,0,9.5,9.5v-21A1.8,1.8,0,0,0,33.7,12.6Z"
            />
            <path
              fill={usedColour()}
              d="M23.2,23.1H2a9.6,9.6,0,0,0,9.6,9.6h3.9v3.7A9.6,9.6,0,0,0,25,46V24.9A1.8,1.8,0,0,0,23.2,23.1Z"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}

JiraIcon.defaultProps = {
  ariaDescription:
    'CLicking on the logo will open a website page in a new browser window',
  size: 50
};

export default JiraIcon;
