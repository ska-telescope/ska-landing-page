import React from 'react';
import { External } from '@services/external/external';

const COL_1 = '#54585B';
const COL_2 = '#F78F2C';
const COL_3 = '#D6522B';
const COL_4 = 'url(#SVGID_1_)';

interface ButtonProps {
  ariaDescription?: string;
  dark?: boolean;
  size: number;
}

function JamaIcon({ ...props }: ButtonProps) {
  const external = External('jira');

  const usedColour = (inValue: string) =>
    props && props.dark ? 'white' : inValue;

  return (
    <svg
      aria-label={external?.label}
      aria-describedby="svg-title svg-description"
      width={props.size}
      height={props.size}
      version="1.1"
      id="layer"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="100 200 100 250"
    >
      <path
        fill={usedColour(COL_2)}
        d="M26.7,377.8c0.1,0.7,0.8,1.1,1.5,1h0.2c37.4-21.5,60.4-33.5,69.9-55.2c4.6-10.3,6.9-21.6,6.6-32.9v-45.9
	c0-0.7-0.6-1.3-1.3-1.3c-0.2,0-0.5,0.1-0.7,0.2l-4,2.3c-0.4,0.2-0.6,0.7-0.6,1.1v43.7c0.2,10.4-1.8,20.6-5.9,30.2
	c-8.9,20.5-32.1,31.6-64.4,50.4c-0.8,0.4-1.3,1.2-1.3,2.1V377.8z M70.8,262.7c-0.5,0.3-0.8,0.8-0.8,1.3L70,288.5
	c0.2,28-19,37-42,50.1c-0.8,0.5-1.2,1.4-1.2,2.3v4.6c0,0.5,0.4,0.9,0.9,0.9c0.2,0,0.3,0,0.4-0.1c28.2-16,48.7-25.2,48.5-59.8
	l0.2-25.6c0-0.5-0.4-0.9-0.9-0.9c-0.2,0-0.3,0-0.5,0L70.8,262.7z M26.7,361.7v-4.6c0-0.9,0.4-1.8,1.3-2.3
	c31.6-18,45.6-25.6,52.4-43.6c4.1-11,4.1-18.5,3.8-55.5c0-0.6,0.3-1.1,0.8-1.4l4.6-2.7c0.4-0.2,1-0.1,1.2,0.3
	c0.1,0.1,0.1,0.3,0.1,0.5c0,37.9,1,47.1-4.2,61.1c-8.3,22.1-27.1,30.8-58.6,49c-0.4,0.2-0.9,0.1-1.2-0.3
	C26.7,362,26.7,361.8,26.7,361.7z"
      />
      <path
        fill={usedColour(COL_3)}
        d="M186.7,389.2l-32.4,18.5c-0.5,0.3-1.1,0.3-1.5,0l-19.4-11c-17.5-10.1-32-8.8-47.4,0l-19.4,11.1
	c-0.5,0.3-1,0.3-1.5,0l-32.4-18.4c-0.6-0.4-0.8-1.1-0.5-1.8c0.1-0.2,0.3-0.4,0.5-0.5c42.4-24.1,54.7-33.9,77-33.7l0,0
	c22.4-0.1,35.7,10.1,77.1,33.7c0.6,0.4,0.8,1.2,0.4,1.9c-0.1,0.2-0.3,0.3-0.4,0.4L186.7,389.2z M192,340.9c0-0.9-0.4-1.8-1.2-2.3
	c-14.5-8.4-27.4-15-35.1-25.9c-8.2-11.7-6.8-21.2-7-48.9c0-0.5-0.3-1.1-0.8-1.3l-32-18.9c-0.6-0.4-1.4-0.2-1.8,0.4
	c-0.1,0.2-0.2,0.4-0.2,0.7c0,52.8-1.5,60.4,6.6,78.9c9.5,21.7,32.5,33.7,69.9,55.1c0.7,0.2,1.4-0.1,1.6-0.8c0,0,0,0,0,0
	c0,0,0,0,0-0.1L192,340.9z"
      />
      <linearGradient
        id="SVGID_1_"
        gradientUnits="userSpaceOnUse"
        x1="363.8355"
        y1="522.5455"
        x2="442.0536"
        y2="522.5455"
        gradientTransform="matrix(1 0 0 1 -250 -211.43)"
      >
        <stop offset="0" stopColor="#F78F2C" />
        <stop offset="0.13" stopColor="#F78F2C" stopOpacity="0.79" />
        <stop offset="0.3" stopColor="#F78F2C" stopOpacity="0.55" />
        <stop offset="0.46" stopColor="#F78F2C" stopOpacity="0.36" />
        <stop offset="0.62" stopColor="#F78F2C" stopOpacity="0.2" />
        <stop offset="0.76" stopColor="#F78F2C" stopOpacity="9.000000e-002" />
        <stop offset="0.88" stopColor="#F78F2C" stopOpacity="2.000000e-002" />
        <stop offset="0.97" stopColor="#F78F2C" stopOpacity="0" />
      </linearGradient>
      <path
        fill={usedColour(COL_4)}
        d="M192,340.9c0-0.9-0.4-1.8-1.2-2.3c-14.5-8.4-27.4-15-35.1-25.9c-8.2-11.7-6.8-21.2-7-48.9
	c0-0.5-0.3-1.1-0.8-1.3l-32-18.9c-0.6-0.4-1.4-0.2-1.8,0.4c-0.1,0.2-0.2,0.4-0.2,0.7c0,52.8-1.5,60.4,6.6,78.9
	c9.5,21.7,32.5,33.7,69.9,55.1c0.7,0.2,1.4-0.1,1.6-0.8c0,0,0,0,0,0c0,0,0,0,0-0.1L192,340.9z"
      />
    </svg>
  );
}

JamaIcon.defaultProps = {
  ariaDescription:
    'CLicking on the logo will open a website page in a new browser window',
  size: 50
};

export default JamaIcon;
