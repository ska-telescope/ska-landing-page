import React from 'react';
import { Routes, Route, useNavigate } from 'react-router-dom';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { env } from '../env';

import SubMenu from '@components/menus/SubMenu/SubMenu';

import Shell from '@pages/Shell/Shell';
import Home from '@pages/Home/Home';

import Error from '@pages/Error/Error';
import Title from '@pages/Title/Title';
import Accessibility from '@pages/Accessibility/Accessibility';

import { callPermissionsApi } from '@services/PermissionsApi/PermissionsApi';
import { Access } from '@services/access/access';
import theme from '@services/theme/theme';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

import { PATH } from '@utils/constants';

function App() {
  const navigate = useNavigate();
  const { application, access, updateAccess, user, themeMode } =
    storageObject.useStore();
  const [userPermissions, setUserPermissions] = React.useState({
    cluster: '',
    role: '',
    service: '',
    telescope: '',
    portal_top_menu_permissions: []
  });
  const getPath = (inValue: string) => {
    return env.REACT_APP_SKA_PORTAL_BASE_URL + inValue;
  };

  React.useEffect(() => {
    if (user && user?.username !== 'Guest') {
      const fetchPermissions = async () => {
        try {
          if (user?.token !== '' && user?.token !== undefined) {
            setUserPermissions(await callPermissionsApi(user.token));
          }
        } catch (error) {
          console.error('Error fetching user permissions:', error);
          // Handle errors appropriately (e.g., display error message)
        }
      };
      fetchPermissions();
    }
  }, [user]);

  React.useEffect(() => {
    if (user && user?.username !== 'Guest') {
      const access = Access(user, userPermissions, !application.content1);
      if (access) {
        updateAccess(access);
      }
      navigate(getPath(PATH.HOM));
    }
  }, [userPermissions, user]);

  const ROUTES = [
    {
      path: PATH.ACC,
      element: (
        <Shell>
          <Accessibility />
        </Shell>
      )
    },

    {
      path: PATH.HOM,
      element: (
        <Shell>
          <Home />
        </Shell>
      )
    },

    {
      path: PATH.ALE,
      element: (
        <Shell>
          <Title title="ale" />
        </Shell>
      )
    },
    {
      path: PATH.CHA,
      element: (
        <Shell>
          <Title title="cha" />
        </Shell>
      )
    },
    {
      path: PATH.LOG,
      element: (
        <Shell>
          <Title title="log" />
        </Shell>
      )
    },
    {
      path: PATH.SHL,
      element: (
        <Shell>
          <Title title="shl" />
        </Shell>
      )
    },
    {
      path: PATH.SUB,
      element: (
        <Shell>
          <Title title="sub" />
        </Shell>
      )
    },
    {
      path: PATH.ASK,
      element: (
        <Shell>
          <Title title="ask" />
        </Shell>
      )
    },
    {
      path: PATH.RSK,
      element: (
        <Shell>
          <Title title="rsk" />
        </Shell>
      )
    },

    {
      path: PATH.RES + '/*',
      element: (
        <Shell>
          <SubMenu pages={access?.menu.res} />
        </Shell>
      )
    },
    {
      path: PATH.OBS + '/*',
      element: (
        <Shell>
          <SubMenu pages={access?.menu.obs} />
        </Shell>
      )
    },
    {
      path: PATH.DEV + '/*',
      element: (
        <Shell>
          <SubMenu pages={access?.menu.dev} />
        </Shell>
      )
    },
    {
      path: PATH.TOO + '/*',
      element: (
        <Shell>
          <SubMenu pages={access?.menu.too} />
        </Shell>
      )
    },

    {
      path: PATH.LNK + '/*',
      element: (
        <Shell>
          <SubMenu pages={access?.menu.lnk} />
        </Shell>
      )
    },

    {
      path: '*',
      element: (
        <Shell>
          <Error err="404" />
        </Shell>
      )
    }
  ];
  return (
    <ThemeProvider theme={theme(themeMode?.mode)}>
      <CssBaseline />
      <Routes>
        {ROUTES.map((ROUTE, index) => {
          return (
            <Route
              key={index}
              path={getPath(ROUTE.path)}
              element={ROUTE.element}
            />
          );
        })}
      </Routes>
    </ThemeProvider>
  );
}

export default App;
