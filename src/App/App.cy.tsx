import React from 'react';
import App from './App';
import { CssBaseline, ThemeProvider } from '@mui/material';
import theme from '@services/theme/theme';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<App />', () => {
  for (const theTheme of THEME) {
    it('Theme ' + theTheme, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <App />
        </ThemeProvider>
      );
    });
  }
});
