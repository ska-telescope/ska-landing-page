import React from 'react';
import { importRemote } from '@module-federation/utilities';
import standardErrorPage from './errorPage';
import checkAvailable from './checkAvailable';
import Loader from '@components/loader/Loader';

interface LoadMFRemoteProps {
  scope: string;
  module: string;
  url: string;
}

export const LoadMFRemoteComponent = (props: LoadMFRemoteProps) => {
  const { scope, module, url } = props;

  return React.lazy(() =>
    importRemote({
      url: async () => Promise.resolve(url),
      remoteEntryFileName: 'remoteEntry.js',
      scope,
      module
    })
  );
};

export function DynamicLoader(props: LoadMFRemoteProps) {
  const { scope, module, url } = props;
  const [scriptAvailable, setScriptAvailable] = React.useState(false);
  const SUFFIX = '/remoteEntry.js';
  const fullurl = url + SUFFIX;
  const ERR_MSG = 'The remote module failed to load from source: ' + fullurl;

  React.useEffect(() => {
    checkAvailable(fullurl, setScriptAvailable);
  }, []);

  if (scriptAvailable) {
    const RemoteComponent = LoadMFRemoteComponent({
      scope: scope,
      module: module,
      url: url
    });
    return (
      <React.Suspense fallback={<Loader />}>
        <RemoteComponent />
      </React.Suspense>
    );
  } else {
    return standardErrorPage(ERR_MSG);
  }
}
