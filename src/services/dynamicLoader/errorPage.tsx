import React from 'react';
import { Grid } from '@mui/material';

export default function standardErrorPage(errMsg: string, devMsg?: string) {
  return (
    <Grid container direction="row" justifyContent="space-between">
      <Grid item xs={4}></Grid>
      <Grid
        item
        xs={4}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        data-testid="errorMessage"
      >
        <h2>Unable to get remote</h2>
      </Grid>
      <Grid item xs={4}></Grid>

      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <h3>{errMsg}</h3>
      </Grid>
      <Grid item xs={2}></Grid>

      <Grid item xs={2}></Grid>
      <Grid
        item
        xs={8}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        {devMsg && <h3>{devMsg}</h3>}
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
}
