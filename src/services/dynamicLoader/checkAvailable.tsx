import axios from 'axios';

async function exists(url: string | URL | Request) {
  const result = await fetch(url, { method: 'HEAD' });
  return result.ok;
}

export default async function checkAvailable(
  url: string,
  setAvailableState: Function
) {
  try {
    setAvailableState(await exists(url));
  } catch (error) {
    setAvailableState(false);
  }
}
