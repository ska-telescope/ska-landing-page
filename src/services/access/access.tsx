import React from 'react';
import { PATH } from '@utils/constants';
import { User } from '@ska-telescope/ska-gui-local-storage';
import {
  TELESCOPE_LOW,
  TELESCOPE_MID
} from '@ska-telescope/ska-gui-components';

// NOTE
//
// In here we are going to use the users token to access the Access API.
//
// This will give us information that we will store and then use to
// focus the experience to the users needs

interface UserPermissions {
  cluster: string;
  role: string;
  service: string;
  telescope: string;
  portal_top_menu_permissions: number[];
}

export function Access(
  user: User,
  userPermissions: UserPermissions,
  mockedAuth: Boolean
) {
  const generateTelescopes = () => {
    let tels: string[] = [];
    if (userPermissions && userPermissions?.telescope) {
      if (
        userPermissions &&
        userPermissions?.telescope?.indexOf(TELESCOPE_MID.code) >= 0
      ) {
        tels.push(TELESCOPE_MID.code);
      }
      if (
        userPermissions &&
        userPermissions?.telescope?.indexOf(TELESCOPE_LOW.code) >= 0
      ) {
        tels.push(TELESCOPE_LOW.code);
      }
      return tels;
    }

    // if (user && user.username.indexOf(TELESCOPE_MID.code) >= 0) {
    tels.push(TELESCOPE_MID.code);
    // }
    // if (user && user.username.indexOf(TELESCOPE_LOW.code) >= 0) {
    tels.push(TELESCOPE_LOW.code);
    // }
    return tels;
  };

  const MENU_BASE = [
    { title: 'menu.hom', path: PATH.HOM },
    { title: 'menu.sub', path: PATH.SUB },
    { title: 'menu.obs.title', path: PATH.OBS },
    { title: 'menu.res.title', path: PATH.RES },
    { title: 'menu.log', path: PATH.LOG },
    { title: 'menu.shl', path: PATH.SHL },
    { title: 'menu.too.title', path: PATH.TOO },
    { title: 'menu.lnk.hom', path: PATH.LNK },
    { title: 'menu.dev.title', path: PATH.DEV }
  ];

  const MENU_ITF = [0, 6, 7];
  const MENU_MAINTAINER = [0, 4, 5, 6, 7];
  const MENU_MAINTAINER_LOW = [0, 3, 4, 5, 6, 7];
  const MENU_MAINTAINER_MID = [0, 3, 4, 5, 6, 7];
  const MENU_OPERATOR = [0, 1, 2, 3, 5, 6, 7];
  const MENU_OPERATOR_LOW = [0, 1, 2, 3, 5, 6, 7];
  const MENU_OPERATOR_MID = [0, 1, 2, 3, 5, 6, 7];
  const MENU_OTHER = [0, 6, 7];
  const MENU_CYPRESS = [0, 1, 2, 3, 4, 5, 6, 7, 8];

  const loopMenu = (inValue: any[]) => {
    return inValue.map((el: number) => {
      return MENU_BASE[el];
    });
  };

  const generateTopMenu = () => {
    if (!mockedAuth) {
      if (userPermissions && userPermissions?.portal_top_menu_permissions) {
        return loopMenu(userPermissions?.portal_top_menu_permissions);
      }
    }

    const DEV = 'developer';
    const MAI = 'maintainer';
    const OPE = 'operator';
    const ITF = 'itf';
    const CYPRESS = 'CYPRESS';

    if (!user) {
      return loopMenu(MENU_OTHER);
    }

    if (user && user?.username.indexOf(DEV) >= 0) {
      return MENU_BASE;
    } else if (user && user?.username.indexOf(MAI) >= 0) {
      return user.username.indexOf(TELESCOPE_MID.code) >= 0
        ? loopMenu(MENU_MAINTAINER_MID)
        : user.username.indexOf(TELESCOPE_LOW.code) >= 0
          ? loopMenu(MENU_MAINTAINER_LOW)
          : loopMenu(MENU_MAINTAINER);
    } else if (user && user?.username.indexOf(OPE) >= 0) {
      return user.username.indexOf(TELESCOPE_MID.code) >= 0
        ? loopMenu(MENU_OPERATOR_MID)
        : user.username.indexOf(TELESCOPE_LOW.code) >= 0
          ? loopMenu(MENU_OPERATOR_LOW)
          : loopMenu(MENU_OPERATOR);
    } else if (user && user?.username.indexOf(ITF) >= 0) {
      return loopMenu(MENU_ITF);
    } else if (user && user?.username.indexOf(CYPRESS) >= 0) {
      return loopMenu(MENU_CYPRESS);
    } else {
      return loopMenu(MENU_OTHER);
    }
  };

  const generateDefaultMenu = () => {
    return [
      { title: 'menu.hom', path: '/' },
      { title: 'menu.too.title', path: PATH.TOO },
      { title: 'menu.lnk.hom', path: PATH.LNK }
    ];
  };

  const generateLinksMenu = () => {
    return [{ title: 'menu.lnk.hom', path: '/' }];
  };

  const generateToolingMenu = () => {
    return [
      { title: 'menu.too.hom', path: '/' },
      { title: 'menu.too.ptt', path: PATH.PTT }
    ];
  };

  const generateDevelopersMenu = () => {
    return [
      { title: 'menu.dev.hom', path: '/' },
      { title: 'menu.dev.run', path: '/charts' },
      { title: 'menu.dev.lnk', path: '/links' },
      { title: 'menu.dev.ask', path: '/angularSkeleton' },
      { title: 'menu.dev.rsk', path: '/reactSkeleton' },
      { title: 'menu.dev.rem', path: '/remoteProgress' }
    ];
  };

  const generateObservationsMenu = () => {
    return [
      { title: 'menu.obs.hom', path: '/' },
      { title: 'menu.obs.sig', path: PATH.SIG }
    ];
  };

  const generateResourcesMenu = () => {
    return [
      { title: 'menu.res.hom', path: '/' },
      { title: 'menu.res.dpd', path: PATH.DPD }
    ];
  };

  const access = {
    telescopes: generateTelescopes(),
    menu: {
      def: generateDefaultMenu(),
      dev: generateDevelopersMenu(),
      lnk: generateLinksMenu(),
      obs: generateObservationsMenu(),
      res: generateResourcesMenu(),
      too: generateToolingMenu(),
      top: generateTopMenu()
    }
  };
  return access;
}
