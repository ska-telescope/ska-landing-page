import { PERMISSIONS_API_URI } from '@utils/constants';

/**
 * Attaches a given access token to a Permissions API call and returns
 * information about the user upon successful response.
 *
 * @param accessToken The access token to use for authorization.
 * @returns A Promise that resolves to the user information object on success,
 *   or rejects with an error object if the API call fails.
 */
export async function callPermissionsApi(accessToken: string): Promise<any> {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;

  headers.append('Authorization', bearer);

  const options = {
    method: 'GET',
    headers
  };

  try {
    const response = await fetch(PERMISSIONS_API_URI, options);

    if (!response.ok) {
      // Handle non-2xx status codes appropriately (e.g., throw an error)
      throw new Error(`API call failed with status: ${response.status}`);
    }

    return await response.json(); // Resolve with user information
  } catch (error) {
    // Handle errors gracefully (e.g., log to console, display error message)
    console.error('Error fetching user information:', error);
    throw error; // Re-throw error for potential upstream handling
  }
}
