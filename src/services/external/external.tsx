import {
  TELESCOPE_LOW,
  TELESCOPE_MID
} from '@ska-telescope/ska-gui-components';
import { storageObject } from '@ska-telescope/ska-gui-local-storage';

const LINKS_STANDARD = [
  { id: 'juypter', label: 'Juypter', url: 'http://localhost:8080/jupyter/' },
  { id: 'kibana', label: 'Kibana', url: 'https://www.elastic.co/kibana/' },
  { id: 'grafana', label: 'Grafana', url: 'https://grafana.com/' },
  { id: 'taranta', label: 'Taranta', url: 'http://localhost:8080/taranta/' }
];

const LINKS_LOW_ITF = [
  {
    id: 'juypter',
    label: 'Juypter',
    url: 'https://k8s.lowitf.internal.skao.int/binderhub/jupyterhub/'
  },
  {
    id: 'kibana',
    label: 'Kibana',
    url: 'https://k8s.stfc.skao.int/kibana/app/logs/stream?logFilter=(language:kuery,query:%27ska.datacentre:%20%22low-itf)'
  },
  {
    id: 'grafana',
    label: 'Grafana',
    url: 'https://k8s.lowitf.internal.skao.int/grafana'
  },
  {
    id: 'taranta',
    label: 'Taranta',
    url: 'https://k8s.lowitf.internal.skao.int/test-environment/taranta'
  }
];

const LINKS_MID_ITF = [
  { id: 'kibana', label: 'Kibana', url: 'https://www.elastic.co/kibana/' }
];

const isITF = (user: any) => {
  return user?.username.indexOf('itf') >= 0;
};

const getData = (array: typeof LINKS_STANDARD, e: string) => {
  return array.find((x) => x.id === e);
};

export const External = (
  inValue: string
): { id: string; label: string; url: string } | null => {
  const { telescope, user } = storageObject.useStore();

  function isTelescope(inValue: string) {
    return telescope && telescope.code && telescope.code.indexOf(inValue) >= 0;
  }

  let result = null;

  if (isITF(user)) {
    if (isTelescope(TELESCOPE_LOW.code)) {
      result = getData(LINKS_LOW_ITF, inValue);
    }
    if (isTelescope(TELESCOPE_MID.code)) {
      result = getData(LINKS_MID_ITF, inValue);
    }
  }

  if (!result) {
    result = getData(LINKS_STANDARD, inValue);
  }
  return !result ? null : result;
};
