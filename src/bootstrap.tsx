import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { createRoot } from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import i18n from '@services/i18n/i18n';
import { StoreProvider } from '@ska-telescope/ska-gui-local-storage';
import App from './App/App';
import AuthProvider from '@services/auth/AuthProvider';
import Loader from '@components/loader/Loader';

const container = document.getElementById('root');
if (container) {
  const root = createRoot(container);

  root.render(
    <React.StrictMode>
      <StoreProvider>
        <React.Suspense fallback={<Loader />}>
          <AuthProvider>
            <I18nextProvider i18n={i18n}>
              <BrowserRouter>
                <App />
              </BrowserRouter>
            </I18nextProvider>
          </AuthProvider>
        </React.Suspense>
      </StoreProvider>
    </React.StrictMode>
  );
}
