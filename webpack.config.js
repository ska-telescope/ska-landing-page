const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const deps = require('./package.json').dependencies;
const Dotenv = require('dotenv-webpack');

const version = require('./package.json').version;

module.exports = () => {
  return {
    entry: './src/index.jsx',
    output: {},

    performance: {
      hints: false,
      maxEntrypointSize: 512000,
      maxAssetSize: 512000
    },

    resolve: {
      alias: {
        '@components': path.resolve(__dirname, 'src/components'),
        '@pages': path.resolve(__dirname, 'src/pages'),
        '@services': path.resolve(__dirname, 'src/services'),
        '@utils': path.resolve(__dirname, 'src/utils')
      },
      extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
      // Add support for TypeScripts fully qualified ESM imports.
      extensionAlias: {
        '.js': ['.js', '.ts'],
        '.cjs': ['.cjs', '.cts'],
        '.mjs': ['.mjs', '.mts']
      },
      fallback: {
        path: require.resolve('path-browserify')
      }
    },

    devServer: {
      port: 4200,
      historyApiFallback: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods':
          'GET, POST, PUT, DELETE, PATCH, OPTIONS',
        'Access-Control-Allow-Headers':
          'X-Requested-With, content-type, Authorization'
      }
    },

    module: {
      rules: [
        {
          test: /\.m?js|\.jsx/,
          type: 'javascript/auto',
          resolve: {
            fullySpecified: false
          }
        },
        {
          test: /\.s[ac]ss$/i,
          use: ['style-loader', 'css-loader', 'sass-loader']
        },
        {
          test: /\.(ts|tsx|js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',
          options: { presets: ['@babel/env', '@babel/preset-react'] }
        },
        {
          test: /\.json$/,
          loader: 'json-loader'
        },
        {
          test: /\.svg$/,
          loader: 'svg-loader'
        },
        {
          test: /\.(jpe?g|png|gif)$/i,
          loader: 'file-loader'
        }
      ]
    },

    devtool: 'source-map',

    plugins: [
      new ModuleFederationPlugin({
        name: 'skaPortal',
        filename: 'remoteEntry.js',
        remotes: {},
        exposes: {},
        shared: {
          ...deps,
          react: {
            eager: true,
            singleton: true,
            requiredVersion: deps['react']
          },
          'react-dom': {
            eager: true,
            singleton: true,
            requiredVersion: deps['react-dom']
          },
          'react-router-dom': {
            eager: true,
            singleton: true,
            requiredVersion: deps['react-router-dom']
          },
          // i18n
          i18next: {
            eager: true,
            singleton: true,
            requiredVersion: deps.i18next
          },
          'react-i18next': {
            eager: true,
            singleton: true,
            requiredVersion: deps['react-i18next']
          },
          'i18next-browser-languagedetector': {
            eager: true,
            singleton: true,
            requiredVersion: deps['i18next-browser-languagedetector']
          },
          'i18next-http-backend': {
            eager: true,
            singleton: true,
            requiredVersion: deps['i18next-http-backend']
          },
          // Material-UI
          '@mui/material': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@mui/material']
          },
          '@mui/icons-material': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@mui/icons-material']
          },
          '@mui/lab': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@mui/lab']
          },
          // Redux
          'react-redux': {
            eager: true,
            singleton: true,
            requiredVersion: deps['react-redux']
          },
          redux: {
            eager: true,
            singleton: true,
            requiredVersion: deps['redux']
          },
          '@reduxjs/toolkit': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@reduxjs/toolkit']
          },
          'redux-saga': {
            eager: true,
            singleton: true,
            requiredVersion: deps['redux-saga']
          },
          'redux-localstorage-simple': {
            eager: true,
            singleton: true,
            requiredVersion: deps['redux-localstorage-simple']
          },
          'redux-dynamic-middlewares': {
            eager: true,
            singleton: true,
            requiredVersion: deps['redux-dynamic-middlewares']
          },
          // SKAO components
          '@ska-telescope/ska-gui-components': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@ska-telescope/ska-gui-components']
          },
          '@ska-telescope/ska-gui-local-storage': {
            requiredVersion: deps['@ska-telescope/ska-gui-local-storage'],
            singleton: true,
            eager: true
          },
          // MS Entra components
          '@azure/msal-browser': {
            requiredVersion: deps['@azure/msal-browser'],
            singleton: true,
            eager: true
          },
          '@azure/msal-react': {
            requiredVersion: deps['@azure/msal-react'],
            singleton: true,
            eager: true
          },
          moment: {
            eager: true,
            singleton: true,
            requiredVersion: deps.moment
          },
          // mixture
          '@emotion/react': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@emotion/react']
          },
          '@emotion/styled': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@emotion/styled']
          },
          moment: {
            eager: true,
            singleton: true,
            requiredVersion: deps.moment
          },
          'react-plotly.js': {
            eager: true,
            singleton: true,
            requiredVersion: deps['react-plotly.js']
          },
          axios: {
            eager: true,
            singleton: true,
            requiredVersion: deps['axios']
          },
          '@module-federation/utilities': {
            eager: true,
            singleton: true,
            requiredVersion: deps['@module-federation/utilities']
          }
        }
      }),
      new HtmlWebPackPlugin({
        inject: true,
        template: './public/index.html'
      }),
      new Dotenv({
        path: '.env'
      }),
      new webpack.EnvironmentPlugin({
        REACT_APP_VERSION: version
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: 'public',
            to: 'dist',
            globOptions: {
              dot: true,
              gitignore: true,
              ignore: ['**/*.html']
            }
          }
        ]
      })
    ]
  };
};
