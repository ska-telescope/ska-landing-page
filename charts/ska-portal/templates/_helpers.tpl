{{/*
Selector labels
*/}}
{{- define "ska-portal.labels" -}}
app.kubernetes.io/name: {{ $.Chart.Name }}
{{- end }}
{{- define "ska-portal.portal.labels" -}}
{{ include "ska-portal.labels" . }}
app: {{ $.Chart.Name }}-portal
{{- end }}

{{- define "ska-portal.loginpermissionsApi.labels" -}}
{{ include "ska-portal.labels" . }}
app: {{ $.Chart.Name }}-permissions-api
{{- end }}

{{/*
set the ingress url path
*/}}
{{- define "ska-portal.path" }}
{{- if .Values.entrypoint.prependByNamespace -}}
/{{ .Release.Namespace }}/{{ .Values.entrypoint.path }}
{{- else if .Values.entrypoint.path -}}
/{{ .Values.entrypoint.path }}
{{- else -}}

{{- end }}
{{- end }}

{{- define "ska-portal.permissionsApi.ingress.path" }}
{{- if .Values.entrypoint.prependByNamespace -}}
/{{ .Release.Namespace }}/{{ .Values.permissionsApi.ingress.path }}
{{- else -}}
/{{ .Values.permissionsApi.ingress.path }}
{{- end }}
{{- end }}