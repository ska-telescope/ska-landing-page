{{/*
set the ingress url path
*/}}
{{- define "ska-portal-low.path" }}
{{- $portal := index .Values "ska-portal" }}
{{- if $portal.entrypoint.prependByNamespace -}}
/{{ .Release.Namespace }}/{{ $portal.entrypoint.path }}
{{- else if $portal.entrypoint.path -}}
/{{ $portal.entrypoint.path }}
{{- else -}}
{{- end }}
{{- end }}
