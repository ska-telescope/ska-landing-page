export function verifyAccessibilityChatAndAlertsAreAvailable() {
  // cy.get('[data-testid="AccessibilityNewOutlinedIcon"]').click();
  // cy.get('[data-testid="accessibilityCardId"]')
  //   .should('be.visible')
  //   .contains('PLACEHOLDER FOR ACCESSIBILITY WORK');
  // cy.get('[data-testid="chatIconButton"]').click();
  // cy.get('[data-testid="menu.cha"]').contains('Chat');
  // cy.get('[data-testid="alertIconButton"]').click();
  // cy.get('[data-testid="menu.ale"]').contains('Alerts');
}

export function verifyUsefulLinks() {
  cy.get('[data-testid="SKAMPI Docs"]').click();
  cy.get('[data-testid="Developers Portal"]').click();
  cy.get('[data-testid="GIT Repository"]').click();
}

export function verifySKAOLogo() {
  // cy.get('[data-testid="skaoLogo"]').click();  // TODO This fails as there are multiple instanes of the skaoLogo on the page.
}

export function verifyDarkMode() {
  cy.get('[data-testid="Brightness7Icon"]').should('be.visible');
}

export function verifyLightMode() {
  cy.get('[data-testid="Brightness4Icon"]').should('be.visible');
}

export function clickDarkMode() {
  cy.get('[data-testid="Brightness7Icon"]').click();
  verifyLightMode();
}

export function clickLightMode() {
  cy.get('[data-testid="Brightness4Icon"]').click();
  verifyDarkMode();
}

/**************************/

export function visitBaseURL() {
  cy.visit('http://localhost:4200');
}

/**************************/

// NOTE that this functionality will need to be changed to provide username and password
export function enterLoginDetails(value) {
  cy.get('[data-testid="emailId"]').type(value);
  cy.get('[data-testid="loginConfirm"').click();
}

export function clickLogin() {
  cy.get('[data-testid="useMSEntraTestId"]').click();
  cy.get('[data-testid="loginButton"]').click();
}

export function clickLoginConfirm() {
  cy.get('[data-testid="loginConfirm"]').click();
}

export function clickLoginCancel() {
  cy.get('[data-testid="loginCancel"]').click();
  isLoggedOut();
}

export function logUserIn(value) {
  clickLogin();
  enterLoginDetails(value);
  isLoggedIn(value);
}

export function beginLogUserInThenCancel(value) {
  cy.get('[data-testid="useMSEntraTestId"]').click();
  cy.get('[data-testid="loginButton"]').click();
  cy.get('[data-testid="emailId"]').type(value);
  clickLoginCancel();
}

export function logUserOut() {
  cy.get('[data-testid="userName"]').click();
  cy.get('[data-testid="logoutButton"]').click();
  cy.get('[data-testid="logoutConfirm"]').click();
  isLoggedOut();
}

/**************************/

export function isLoggedIn(inValue) {
  cy.get('[data-testid="userName"]').should('be.visible').contains(inValue);
}

export function isLoggedOut() {
  cy.get('[data-testid="loginButton"]').should('be.visible');
}
