import {
  clickDarkMode,
  clickLightMode,
  logUserIn,
  logUserOut,
  verifySKAOLogo,
  visitBaseURL
} from '../commonFunctionality';
import {
  developersMenuItem,
  linksMenuItem,
  logisticsMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  shiftLogsMenuItem,
  subArrayMenuItem,
  toolingMenuItem
} from '../menuFunctionality';

context('Check that all the menu items are available', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Log in as CYPRESS USER', () => {
    logUserIn('CYPRESS');
  });

  after(() => {
    verifySKAOLogo();
    clickDarkMode();
    clickLightMode();
    // TODO : Why is this failing ?
    // cy.get('[data-testid="telescopeSelectorIdlow"]').contains('SKA LOW');
    // cy.get('[data-testid="telescopeSelectorIdmid"]').contains('SKA MID');
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    developersMenuItem();
    logUserOut();
  });
});
