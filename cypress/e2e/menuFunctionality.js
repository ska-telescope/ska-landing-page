import { verifyUsefulLinks } from './commonFunctionality';

// HOME

export function subArrayMenuItem() {
  cy.get('[data-testid="ViewArrayOutlinedIcon"]').click();
  cy.get('[data-testid="menu.sub"]').contains('Subarray');
}

export function observationsMenuItem() {
  cy.get('[data-testid="VisibilityOutlinedIcon"]').click();
  cy.get('[data-testid="menu.obs.hom"]').contains('Observations home');
  //TODO cy.get('[data-testid="Signal Metrics"]').type('{enter}');
  //TODO: Implement simple signal display verification
}

export function resourcesMenuItem() {
  cy.get('[data-testid="SettingsInputCompositeOutlinedIcon"]').click();
  cy.get('[data-testid="menu.res.hom"]').contains('Resources home');
  // cy.get('[data-testid="LegendToggleOutlinedIcon"]').click();  // TODO The SDP Monitoring remote is not in development at the moment.
  // cy.get('[data-testid="errorMessage"]')
  //   .contains(
  //     'Add notes here summarizing the functionality that is not available'
  //   )
  //   .should('be.visible');
  cy.get('[data-testid="Inventory2OutlinedIcon"]').click();
}

export function logisticsMenuItem() {
  cy.get('[data-testid="DnsIcon"]').click();
  cy.get('[data-testid="menu.log"]').contains('Logistics');
}

export function shiftLogsMenuItem() {
  cy.get('[data-testid="HistoryOutlinedIcon"]').click();
  cy.get('[data-testid="menu.shl"]').contains('Shift Logs');
  cy.get('[data-testid="overview.shl"]')
    .should('be.visible')
    .contains(
      'Allows for search, view and entry of information that needs to be stored in relation to an event and/or observation that happens during the a shift. Note that some entries into a shift log could be generated automatically and/or could be linked to an observation log.'
    );
}

// LINKS

export function linksMenuItem() {
  // cy.get('[data-testid="OpenInNewIcon"]').click();   // TODO These tests need to be updated to take into account what a user should be able to see.
  // cy.get('[data-testid="kibana"]').click();
  // cy.get('[data-testid="jupyter"]').click();
  // cy.get('[data-testid="grafana"]').click();
  // cy.get('[data-testid="taranta"]').click();
}

// TOOLING

export function toolingMenuItem() {
  // cy.get('[data-testid="BuildCircleOutlinedIcon"]').click();    // TODO These tests need to be updated to take into account what a user should be able to see.
  // TODO cy.get('[data-testid="toolHomeTitle"]').contains('Tooling Home');
  // TODO cy.get('[data-testid="toolHomeDesc"]').contains('Home page for SKAO tooling');
  // cy.get('[data-testid="EditCalendarOutlinedIcon"]').click();    // TODO These tests need to be updated to take into account what a user should be able to see.
}

// DEVELOPER

export function developersMenuItem() {
  cy.get('[data-testid="CodeIcon"]').click();
  cy.get('[data-testid="devHomeTitle"]').contains('Developers Home');
  cy.get('[data-testid="devHomeDesc"]').contains(
    'This will contain any and all generic information that may be of use to developers as well as links to remote applications like the reactSkeleton. This is NOT intended for general usage'
  );
  //
  cy.get('[data-testid="SchemaIcon"]').click();
  cy.get('[data-testid="devRunTitle"]').contains('Running Charts');
  cy.get('[data-testid="runningTreeIcon"]').click();
  // TODO cy.get('[text="ska-tango-base 0.3.5"]').should('be.visible');
  cy.get('[data-testid="LinkIcon"]').click();
  cy.get('[data-testid="devLnkTitle"]').contains('Useful Links');
  verifyUsefulLinks();
  cy.get('[data-testid="angular"]').click();
  cy.get('[data-testid="errorMessage"]').contains('Unable to get remote');
  cy.get('[data-testid="react"]').click();
  cy.get('[data-testid="errorMessage"]').contains('Unable to get remote');
  cy.get('[data-testid="CloudSyncOutlinedIcon"]').click();
  cy.get('[data-testid="implementProgress"]').contains(
    'Implementation progress for Dynamic Remotes'
  );
}
