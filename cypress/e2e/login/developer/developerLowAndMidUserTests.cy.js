import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  developersMenuItem,
  logisticsMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  shiftLogsMenuItem,
  subArrayMenuItem
} from '../../menuFunctionality';

const username = 'developer-mid-low';

context('User journeys as a DEVELOPER (BOTH)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Log in as DEVELOPER (BOTH) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as DEVELOPER (BOTH)', () => {
    logUserIn(username);
  });

  after(() => {
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    developersMenuItem();
    verifySKAOLogo();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
