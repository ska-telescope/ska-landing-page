import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  developersMenuItem,
  logisticsMenuItem,
  shiftLogsMenuItem,
  subArrayMenuItem,
  observationsMenuItem,
  resourcesMenuItem
} from '../../menuFunctionality';

const username = 'developer-low';

context('User journeys as a DEVELOPER (LOW)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as DEVELOPER (LOW) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as DEVELOPER (LOW)', () => {
    logUserIn(username);
  });

  after(() => {
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    developersMenuItem();
    verifySKAOLogo();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
