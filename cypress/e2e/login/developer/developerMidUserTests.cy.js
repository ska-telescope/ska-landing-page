import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  subArrayMenuItem,
  developersMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  logisticsMenuItem,
  shiftLogsMenuItem
} from '../../menuFunctionality';

const username = 'developer-mid';

context('User journeys as a DEVELOPER (MID)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as DEVELOPER (MID) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as DEVELOPER (MID)', () => {
    logUserIn(username);
    verifySKAOLogo();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    developersMenuItem();
    logUserOut();
  });
});
