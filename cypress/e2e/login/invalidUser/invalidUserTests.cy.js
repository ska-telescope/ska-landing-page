import { visitBaseURL } from '../../commonFunctionality';

context('User journey as invalid user', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  // TODO : Annotated out whilst gui-components is updated to support this.

  it('Verify invalid user cannot access application', () => {
    // cy.get('[data-testid="login"]').click(); // TODO
    // cy.get('[data-testid="email"]').type('testUser');
    // cy.get('[data-testid="password"]').type('testing');
    // cy.get('[data-testid="loginConfirm"]').click();
    // cy.get('[data-testid="infoLoginError"]').should('be.visible');
  });
});
