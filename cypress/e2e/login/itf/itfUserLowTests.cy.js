import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import { linksMenuItem, toolingMenuItem } from '../../menuFunctionality';

const username = 'itf-low';

context('User journeys as an ITF User (LOW)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as an ITF User (LOW) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as ITF User (LOW)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
