import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import { linksMenuItem, toolingMenuItem } from '../../menuFunctionality';

const username = 'itf-mid-low';

context('User journeys as an ITF User (BOTH)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as an ITF User (BOTH) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as ITF User (BOTH)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
