import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import { linksMenuItem, toolingMenuItem } from '../../menuFunctionality';

const username = 'itf-mid';

context('User journeys as an ITF User (MID)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as an ITF User (MID) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as ITF User (MID)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
