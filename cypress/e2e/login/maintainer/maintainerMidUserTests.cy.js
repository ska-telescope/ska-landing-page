import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  logisticsMenuItem,
  resourcesMenuItem,
  shiftLogsMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'maintainer-mid';

context('User journeys as a Maintainer (MID)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as a MAINTAINER (MID) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as MAINTAINER (MID)', () => {
    logUserIn('maintainer-mid');
  });

  after(() => {
    verifySKAOLogo();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
