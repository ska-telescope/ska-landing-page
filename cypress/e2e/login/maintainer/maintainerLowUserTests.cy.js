import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  logisticsMenuItem,
  resourcesMenuItem,
  shiftLogsMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'maintainer-low';

context('User journeys as a Maintainer (LOW)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as a MAINTAINER (LOW) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as MAINTAINER (LOW)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
