import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  logisticsMenuItem,
  resourcesMenuItem,
  shiftLogsMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'maintainer-mid-low';

context('User journeys as a Maintainer (BOTH)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as a MAINTAINER (BOTH) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as MAINTAINER (BOTH)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    resourcesMenuItem();
    logisticsMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
