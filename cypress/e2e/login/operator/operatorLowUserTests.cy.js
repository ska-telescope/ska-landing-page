import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  shiftLogsMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  subArrayMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'operator-low';

context('User journeys as an Operator (LOW)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as OPERATOR (LOW) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as OPERATOR (LOW)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
