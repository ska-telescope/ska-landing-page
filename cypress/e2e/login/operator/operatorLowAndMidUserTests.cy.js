import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  shiftLogsMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  subArrayMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'operator-mid-low';

context('User journeys as an Operator (BOTH)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as OPERATOR (BOTH) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as OPERATOR (BOTH)', () => {
    logUserIn('operator-mid-low');
  });

  after(() => {
    verifySKAOLogo();
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
