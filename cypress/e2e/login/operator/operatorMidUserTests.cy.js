import {
  beginLogUserInThenCancel,
  logUserIn,
  logUserOut,
  verifyAccessibilityChatAndAlertsAreAvailable,
  verifyDarkMode,
  verifySKAOLogo,
  visitBaseURL
} from '../../commonFunctionality';
import {
  linksMenuItem,
  shiftLogsMenuItem,
  observationsMenuItem,
  resourcesMenuItem,
  subArrayMenuItem,
  toolingMenuItem
} from '../../menuFunctionality';

const username = 'operator-mid';

context('User journeys as an Operator (MID)', () => {
  beforeEach(() => {
    visitBaseURL();
  });

  it('Begin log in as OPERATOR (MID) then cancel', () => {
    beginLogUserInThenCancel(username);
  });

  it('Log in as OPERATOR (MID)', () => {
    logUserIn(username);
  });

  after(() => {
    verifySKAOLogo();
    subArrayMenuItem();
    observationsMenuItem();
    resourcesMenuItem();
    shiftLogsMenuItem();
    linksMenuItem();
    toolingMenuItem();
    verifyDarkMode();
    verifyAccessibilityChatAndAlertsAreAvailable();
    logUserOut();
  });
});
